--DDL

--CREATE DATABASE
CREATE DATABASE db_kampus2

use db_penerbit

--create table
create table mahasiswa (
id bigint primary key identity(1,1),
name varchar(50) not null,
address varchar(50) not null,
email varchar (255) null
)

--create view
create view vwMahasiswa
as 
select id, name, address, email from mahasiswa
---------------------

--select view
select * from vwMahasiswa

--Alter add column
alter table mahasiswa add [description] varchar(255)

--alter drop column
alter table mahasiswa drop column [description]

--table alter table (mengubah column yang sudah ada)
alter table mahasiswa alter column email varchar(100)
----------------------
--alter view
--alter view [NamaViewLama] to [NamaViewBaru]


--------------------------------------

--drop database
drop database db_kampus2

--drop table
drop table bioadata

--drop view
drop view [NamaView]
drop view vwMahasiswa

--------------------

--DML

--insert data
insert into mahasiswa(name, address, email) 
values ('Marchel', 'Medan', 'Marchel@gmail.com')

insert into mahasiswa(name, address, email) 
values ('Toni', 'Garut', 'Toni@gmail.com')

insert into mahasiswa(name, address, email)
values 
('Marchel', 'Medan2', 'Marchel@gmail.com'),
('Toni', 'Garut', 'Toni@gmail.com'),
('Isni', 'Cimahi', 'isni@gmail.com')

insert into mahasiswa(name, address, email)
values ('Isni', 'Cimahi', 'isni@gmail.com') 

--select data
select id, name, address, email from mahasiswa
select * from mahasiswa

--update data
update mahasiswa set name='Isni Dwitiniardi',address='Sumedang' where name='isni'

--Delete
delete from mahasiswa where id=5

create table biodata (
id bigint primary key identity(1,1),
dob datetime not null,
kota varchar(100) null
)

alter table biodata add mahasiswa_id bigint not null
alter table biodata alter column mahasiwa_id bigint not null


--join table 
select mhs.name, bio.kota, bio.dob
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.name = 'toni'

insert into biodata (dob, kota, mahasiswa_id)
values 
('2000-03-15','Jaksel',01),
('2001-04-19','Jakbar',02),
('1998-08-05','Jakpus',03)

update biodata set kota='Kalibata', mahasiswa_id=4 where kota='jakpus'

select * from biodata


alter table biodata alter column dob date not null


--join table 
select mhs.id as ID, mhs.name, bio.kota, bio.dob,
month(bio.dob)BulanLahir,year(bio.dob)TahunLahir,day(bio.dob)TanggalLahir
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
where mhs.name = 'toni' or bio.kota='Daan Mogot'

--order by
select *
from mahasiswa mhs
join biodata bio on mhs.id = bio.mahasiswa_id
order by mhs.id asc, mhs.name desc

--select top
select top 1 * from mahasiswa order by name desc

--between
select * from mahasiswa where id between 1 and 3
select * from mahasiswa where id >=1 and id <= 3

--Like 
select * from mahasiswa where name like 'm%'
select * from mahasiswa where name like '%i'
select * from mahasiswa where name like '%dwi%'
select * from mahasiswa where name like '_o%'
select * from mahasiswa where name like '__n%'
select * from mahasiswa where name like 't_%'
select * from mahasiswa where name like 't___%'
select * from mahasiswa where name like 'm%l'


--Group By
select count(id), name from mahasiswa group by name

--having
select count(id), name 
from mahasiswa 
group by name 
having count(id) > 1

--distinct
select distinct name from mahasiswa

--substring
select substring ('sql tutorial', 1, 3) as Judul 

--charIndex
select CHARINDEX('t', 'Customer') as Judul


--Data Lenght
select DATALENGTH('akumau.istirahat') 

--alter table mahasiswa add panjang2 smallint
--update mahasiswa set panjang2=100
--alter table mahasiswa alter column panjang2 smallint not null
--alter table mahasiswa drop column panjang2

--update mahasiswa set panjang=48 where address='medan'
--update mahasiswa set panjang=86 where name='Toni'
--update mahasiswa set panjang=117 where name like '%isni%'
--update mahasiswa set panjang=50 where id=6

select * from mahasiswa
select * from mahasiswa order by panjang asc --desc

--case when
select id,name,address,panjang, 
case when panjang < 50 then 'Pendek'
when panjang <= 100 then 'sedang'
else 'tinggi'
end as TinggiBadan
from mahasiswa

--concat
select CONCAT('SQL', ' is', ' Fun!')

select 'SQL' + 'is' + 'fun!'


create table penjualan(
id bigint primary key identity(1,1),
nama varchar(50) not null,
harga int not null
)

insert into penjualan(nama, harga)
values
('Indomie', 1500),
('Close-Up', 3500),
('Pepsoden', 3000),
('Brush Formula', 2500),
('Roti Manis', 1000),
('Gula', 3500),
('Sarden', 4500),
('Rokok Sampurna', 11000),
('Rokok 234', 11000)

select * from penjualan
--operator aritmatika
select *, harga*100 as TotalHarga from penjualan 

sp_columns 'tblPengarang'

sp_help'tblPengarang'