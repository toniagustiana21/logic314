--TUGAS SQL DAY 03

create database DB_Sales

use DB_sales

create table SALESPERSON(
ID int primary key identity(1,1),
NAME varchar(50) not null,
BOD date not null,
SALARY decimal(18,2) not null
)

insert into SALESPERSON(NAME, BOD, SALARY)
values
('Abe', '1998/11/9', 140000),
('Bob', '1978/11/9', 44000),
('Chris', '1983/11/9', 40000),
('Dan', '1980/11/9', 52000),
('Ken', '1977/11/9', 115000),
('Joe', '1990/11/9', 38000)

create table ORDERS(
ID int primary key identity(1,1),
ORDER_DATE date not null,
CUST_ID int null,
SALESPERSON_ID int not null,
AMOUNT decimal(18,2) not null
)

insert into ORDERS(ORDER_DATE, CUST_ID, SALESPERSON_ID, AMOUNT)
values
('2020-8-2', 4, 2, 540),
('2021-1-22', 4, 5, 1800),
('2019-7-14', 9, 1, 460),
('2018-1-29', 7, 2, 2400),
('2021-2-3', 6, 4, 600),
('2020-3-2', 6, 4, 720),
('2021-5-6', 9, 4, 150)

--soal a (nama sales yang memiliki order lebih dari 1)
select sal.NAME, COUNT(ord.SALESPERSON_ID) 
from SALESPERSON as sal join ORDERS as ord
on sal.ID = ord.SALESPERSON_ID
group by sal.NAME
having COUNT(ord.SALESPERSON_ID) > 1

--soal b (nama sales yang total amount ordernya diatas 1000)
select sal.NAME, sum(ord.AMOUNT) as total_amount
from SALESPERSON as sal join ORDERS as ord
on sal.ID = ord.SALESPERSON_ID
group by sal.NAME
having sum(ord.AMOUNT) > 1000

--soal c(nama sales, umur, gaji dan total amount yang tahun ordernya >= 2020, berurutan asc)
select sal.NAME, 
DATEDIFF(YEAR, sal.bod, GETDATE()) as umur, 
sal.SALARY, 
sum(ord.AMOUNT) as total_amount 
from SALESPERSON as sal join ORDERS as ord
on sal.ID = ord.SALESPERSON_ID
where year(ord.ORDER_DATE) >= 2020
group by sal.NAME, sal.SALARY, sal.BOD
order by umur asc

--soal d (rata" total amount masing-masing sales)
select top 1 sal.NAME, avg(ord.AMOUNT) as RataRata_Total_Amount
from SALESPERSON as sal 
join ORDERS as ord on sal.ID = ord.SALESPERSON_ID
group by sal.NAME
order by RataRata_Total_Amount asc

--soal e (bonus bila order lebih dari 2 dan total order lebih dari 1000)
select sal.NAME, 
COUNT(ord.ID) [total order], 
sum(ord.AMOUNT) [total amount],
sal.SALARY * 0.3 [bonus]
from SALESPERSON as sal 
join ORDERS as ord on sal.ID = ord.SALESPERSON_ID
group by sal.NAME, sal.SALARY
having COUNT(ord.ID) > 2 and sum(ord.AMOUNT) > 1000

--soal f (data sales yang belum memiliki orderan)
select sal.NAME 
from SALESPERSON sal
left join ORDERS ord on sal.ID = ord.SALESPERSON_ID
where ord.SALESPERSON_ID is null

--soal g (potong gaji jika tidak memiliki orderan)
select sal.NAME,
sal.SALARY * 0.02 [potongan],
sal.SALARY [gaji_kotor],
sal.SALARY - (sal.SALARY * 0.02) [gaji_bersih]
from SALESPERSON sal 
left join ORDERS ord on sal.ID = ord.SALESPERSON_ID
where ord.SALESPERSON_ID is null