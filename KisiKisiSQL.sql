--Kisi-kisi

create database db_KisiKisi
use db_Kisikisi

create table assigment(
id int primary key identity(1,1),
name varchar(50) not null,
marks varchar(50) null,
grade int not null
)



insert into assigment(name, grade)
values
('Isni', 85),
('Laudry', 75),
('Bambang', 40),
('Anwar', 91),
('Alwi', 70),
('Fulan', 50)

select name, marks,
case 
when marks > 90 then 'A+'
when marks > 70 then 'A'
when marks > 60 then 'B'
when marks > 40 then 'C'
else 'Fail'
end as [grade] 
from assigment

insert into assigment(name, marks)
values
('Data', 100)

delete from assigment where id = 7

select * from assigment

create table employee(
id int primary key identity(1,1),
name varchar(50) not null,

)
alter table departement drop column salary
alter table employee add salary decimal(18,2)

insert into employee(name)
values
('isni'),
('laudry'),
('firdha'),
('asti')

create table departement(
id int primary key identity(1,1),
employee_id int not null,
name varchar(50) not null,
salary decimal(18,2) not null
)

alter table employee add departement_id int

alter table departement drop column employee_id


insert into departement(name)
values
('HRD'),('Accounting'),('IT'),('Finance')

select * from departement
select * from employee

update employee set departement_id=1,salary=100000 where id = 1
update employee set departement_id=2,salary=50000 where id = 2
update employee set departement_id=3,salary=200000 where id = 3
update employee set departement_id=4,salary=250000 where id = 4

select dep.name [nama_departemen], AVG(emp.salary) 
from employee as emp 
join departement as dep on emp.departement_id = dep.id
group by dep.name
order by AVG(emp.salary) desc

select ROUND(CAST(AVG(10) as decimal(18,4)), 4)

select convert(int, 10)
select CAST(10 as int)