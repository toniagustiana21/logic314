--TUGAS SQL DAY 03 VERSI 2


create database DB_HR

use DB_HR

create table tb_karyawan(
id bigint primary key,
nip varchar(50) not null,
nama_depan varchar(50) not null,
nama_belakang varchar(50) not null,
jenis_kelamin varchar(50) not null,
agama varchar(50) not null,
tempat_lahir varchar(50) not null,
tgl_lahir date null,
alamat varchar(100) not null,
pendidikan_terakhir varchar(50) not null,
tgl_masuk date null
)

insert into tb_karyawan(id, nip, nama_depan, nama_belakang, jenis_kelamin, agama, tempat_lahir, tgl_lahir, alamat, pendidikan_terakhir, tgl_masuk)
values
(1, '001', 'Hamidi', 'Samsudin', 'Pria', 'Islam', 'Sukabumi', '1977-04-21', 'Jl. Sudirman No. 12', 'S1 Teknik Mesin', '2015-12-07'),
(3, '003', 'Paul', 'Christian', 'Pria', 'Kristen', 'Ambon', '1980-05-27', 'Jl. Veteran No. 4', 'S1 Pendidikan Geografi', '2014-01-12'),
(2, '002', 'Ghandi', 'Wamida', 'Wanita', 'Islam', 'Palu', '1992-01-12', 'Jl. Rambutan No. 22', 'SMA Negeri 02 Palu', '2014-12-01')

select * from tb_karyawan


create table tb_divisi (
id bigint primary key identity(1,1),
kd_divisi varchar(50) not null,
nama_divisi varchar(50) not null
)

insert into tb_divisi(kd_divisi, nama_divisi)
values
('GD', 'Gudang'),
('HRD', 'HRD'),
('KU', 'Keuangan'),
('UM', 'Umum')

create table tb_jabatan(
id bigint primary key,
kd_jabatan varchar(50) not null,
nama_jabatan varchar(50) not null,
gaji_pokok numeric null,
tunjangan_jabatan numeric null
)

insert into tb_jabatan(id, kd_jabatan, nama_jabatan, gaji_pokok, tunjangan_jabatan)
values
(1, 'MGR', 'Manager', 5500000, 1500000),
(2, 'OB', 'Office Boy', 1900000, 200000),
(3, 'ST', 'Staff', 3000000, 750000),
(4, 'WMGR', 'Wakil Manager', 4000000, 1200000)

create table tb_pekerjaan(
id bigint primary key,
nip varchar(50) not null,
kode_jabatan varchar(50) not null,
kode_divisi varchar(50) not null,
tunjangan_kinerja numeric null,
kota_penempatan varchar(50) null
)

insert into tb_pekerjaan(id, nip, kode_jabatan, kode_divisi, tunjangan_kinerja, kota_penempatan)
values
(1, '001', 'ST', 'KU', '750000', 'Cianjur'),
(2, '002', 'OB', 'UM', '350000', 'Sukabumi'),
(3, '003', 'MGR', 'HRD', '1500000', 'Sukabumi')


select * from tb_divisi
select * from tb_jabatan
select * from tb_karyawan
select * from tb_pekerjaan

--soal no 1
select (nama_depan + ' ' +nama_belakang) [nama_lengkap],
jbt.nama_jabatan,
jbt.tunjangan_jabatan + jbt.gaji_pokok [gaji_tunjangangan]
from tb_karyawan as kry join tb_pekerjaan as pkj on kry.nip = pkj.nip
join tb_jabatan as jbt on pkj.kode_jabatan = jbt.kd_jabatan
where jbt.tunjangan_jabatan + jbt.gaji_pokok < 5000000

--soal no 2
select concat(nama_depan, ' ', nama_belakang)[Nama_Lengkap],
jbt.nama_jabatan,
dvs.nama_divisi,
jbt.gaji_pokok + jbt.tunjangan_jabatan + pkj.tunjangan_kinerja[total_gaji],
(jbt.gaji_pokok + jbt.tunjangan_jabatan + pkj.tunjangan_kinerja) * (0.05)[Pajak],
(jbt.gaji_pokok + jbt.tunjangan_jabatan + pkj.tunjangan_kinerja) - ((jbt.gaji_pokok + jbt.tunjangan_jabatan + pkj.tunjangan_kinerja) * (0.05))[gaji_bersih]
from tb_karyawan as kry join tb_pekerjaan as pkj on kry.nip = pkj.nip
join tb_jabatan as jbt on pkj.kode_jabatan = jbt.kd_jabatan
join tb_divisi as dvs on pkj.kode_divisi = dvs.kd_divisi
where kry.jenis_kelamin = 'pria' and pkj.kota_penempatan not like 'sukabumi'

--soal no 3
select kry.nip,
CONCAT(nama_depan, ' ', nama_belakang)[nama_lengkap],
jbt.nama_jabatan,
dvs.nama_divisi,
(jbt.gaji_pokok + jbt.tunjangan_jabatan + pkj.tunjangan_kinerja) * 7 * (0.25)[bonus] 
from tb_karyawan as kry join tb_pekerjaan as pkj on kry.nip = pkj.nip
join tb_jabatan as jbt on pkj.kode_jabatan = jbt.kd_jabatan
join tb_divisi as dvs on pkj.kode_divisi = dvs.kd_divisi

--soal no 4
select kry.nip,
concat(nama_depan,' ',nama_belakang)[nama_lengkap],
jbt.nama_jabatan,
dvs.nama_divisi,
(jbt.gaji_pokok + jbt.tunjangan_jabatan + pkj.tunjangan_kinerja)[total_gaji],
(jbt.gaji_pokok + jbt.tunjangan_jabatan + pkj.tunjangan_kinerja) * 0.05 [infak]
from tb_karyawan as kry join tb_pekerjaan as pkj on kry.nip = pkj.nip
join tb_jabatan as jbt on pkj.kode_jabatan = jbt.kd_jabatan
join tb_divisi as dvs on pkj.kode_divisi = dvs.kd_divisi
where jbt.nama_jabatan = 'manager'

--soal no 5
select kry.nip,
CONCAT(nama_depan,' ',nama_belakang)[nama_lengkap],
jbt.nama_jabatan,
kry.pendidikan_terakhir,
2000000[tunjangan_pendidikan],
jbt.gaji_pokok + jbt.tunjangan_jabatan + 2000000[total_gaji]
from tb_karyawan as kry join tb_pekerjaan as pkj on kry.nip = pkj.nip
join tb_jabatan as jbt on pkj.kode_jabatan = jbt.kd_jabatan
where kry.pendidikan_terakhir like 's1%'

--soal no 6
select kry.nip,
CONCAT(kry.nama_depan,' ', kry.nama_belakang),
jbt.nama_jabatan,
dvs.nama_divisi,
case
	when jbt.kd_jabatan = 'mgr' then 0.25 * (jbt.gaji_pokok + jbt.tunjangan_jabatan + pkj.tunjangan_kinerja) * 7
	when jbt.kd_jabatan = 'st' then 0.25 * (jbt.gaji_pokok + jbt.tunjangan_jabatan + pkj.tunjangan_kinerja) * 5
	else 0.25 * (jbt.gaji_pokok + jbt.tunjangan_jabatan + pkj.tunjangan_kinerja) * 2
	end as bonus
from tb_karyawan as kry join tb_pekerjaan as pkj on kry.nip = pkj.nip
join tb_jabatan as jbt on pkj.kode_jabatan = jbt.kd_jabatan
join tb_divisi as dvs on pkj.kode_divisi = dvs.kd_divisi

--soal no 7
alter table tb_karyawan
add constraint nip_unique unique(nip)

--soal no 8
create index index_nip 
on tb_karyawan (nip)

--soal no 9
select CONCAT(nama_depan, ' ',upper(nama_belakang)) 
from tb_karyawan where nama_belakang like 'w%'

--soal no 10
select CONCAT(kry.nama_depan, ' ', kry.nama_belakang),
kry.tgl_masuk,
jbt.nama_jabatan,
dvs.nama_divisi,
jbt.gaji_pokok + jbt.tunjangan_jabatan + pkj.tunjangan_kinerja[total gaji],
datediff(year)
(jbt.gaji_pokok + jbt.tunjangan_jabatan + pkj.tunjangan_kinerja) * 0.10[bonus]
from tb_karyawan as kry join tb_pekerjaan as pkj on kry.nip = pkj.nip
join tb_jabatan as jbt on pkj.kode_jabatan = jbt.kd_jabatan
join tb_divisi as dvs on pkj.kode_divisi = dvs.kd_divisi
