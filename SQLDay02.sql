--SQL Day 02

--Cast
select cast(10 as decimal(18,4))
select cast('10' as int)
select cast('2023-03-16' as date) as tanggal

select GETDATE(), GETUTCDATE()

--convert
select convert(decimal(18,4), 10)
select convert(int, '10')
select convert(int, 10.65)
select convert(datetime, '2023-03-16')

--dateadd
select DATEADD(YEAR,2,GETDATE()), dateadd(MONTH,3,GETDATE()), DATEADD(DAY,5,GETDATE())

--datediff
select DATEDIFF(day, '2023-03-16','2023-03-25'), DATEDIFF(MONTH, '2023-03-16','2024-06-16'),
DATEDIFF(YEAR, '2023-03-16','2030-03-16')

--sub query
select name,address,email,panjang from mahasiswa
where panjang = (select max(panjang) from mahasiswa)

insert into [dbo].[mahasiswa](name,address,email,panjang)
select name, address,email,panjang from mahasiswa

select * from mahasiswa

--create index
create index index_name 
on mahasiswa (name)

create index index_address_email
on mahasiswa(address,email)

select * from mahasiswa

--unique index
create unique index uniqueindex_address 
on mahasiswa(address)

--drop index
drop index index_address_email on mahasiswa

--drop unique index
drop index uniqueindex_address on mahasiswa

select * from mahasiswa
select * from biodata

--add primary key
alter table mahasiswa 
add constraint pk_id_address primary key(id, address)

--drop primary key
alter table mahasiswa
drop constraint PK__mahasisw__3213E83F45E5F4E9

alter table mahasiswa
drop constraint pk_address

--add unique constraint
alter table mahasiswa 
add constraint unique_panjang unique(panjang)

update mahasiswa set panjang=null where id=6

--drop unique
alter table mahasiswa 
drop constraint unique_address