﻿using System;

namespace Logic01
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //Konversi();
            //operatorAritmatika();
            //modulus();
            //optPenugasan();
            //optPerbandingan();
            //optLogika();
            penjumlahan();
            Console.ReadKey(); 
        }
        static void penjumlahan()
        {
            int mangga, apel;
            Console.WriteLine("--Method Return Type--");
            Console.Write("Jumlah Mangga = ");
            mangga = Convert.ToInt32(Console.ReadLine());
            Console.Write("Jumlah Apel = ");
            apel = int.Parse(Console.ReadLine());

            int hasil = HitungJumlah(mangga, apel);

            Console.WriteLine($"Hasil mangga + apel = {hasil}");
        }

        static int HitungJumlah(int mangga, int apel)
        {
            int hasil = 0;
            hasil = mangga + apel;
            return hasil;
        }

        static void optLogika()
        {
            Console.WriteLine("--Operator Logika--");

            Console.Write("Masukkan Umur = ");
            int umur = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Password = ");
            string password = Console.ReadLine();

            bool isAdult = umur > 18; //pernyataan 1
            bool isPasswordvalid = password == "admin"; // pernyataan 2

            //menggukana logika AND
            if(isAdult && isPasswordvalid)
            {
                Console.WriteLine("Anda sduah dewasa dan password valid");
            }
            else if(isAdult && !isPasswordvalid)
            {
                Console.WriteLine("Anda sudah dewasa dan password invalid");
            }
            else if(!isAdult && isPasswordvalid)
            {
                Console.WriteLine("Anda belum dewasa dan password valid");
            }
            else
            {
                Console.WriteLine("Anda Belum dewasa password invalid");
            }
        }
        static void optPerbandingan()
        {
            int mangga, apel = 0;

            Console.WriteLine("--Operator Perbandingan--");

            Console.Write("Jumlah Mangga = ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("Jumlah Apel = ");
            apel = int.Parse(Console.ReadLine());

            Console.WriteLine("Hasil Perbandingan :");
            Console.WriteLine($"mangga > apel : {mangga > apel}");
            Console.WriteLine($"mangga >= apel : {mangga >= apel}");
            Console.WriteLine($"mangga < apel : {mangga < apel}");
            Console.WriteLine($"mangga <= apel : {mangga <= apel}");
            Console.WriteLine($"mangga == apel : {mangga == apel}");
            Console.WriteLine($"mangga != apel : {mangga != apel}");

            if (mangga > apel)
            {
                Console.WriteLine("mangga lebih banyak");
            }
            else
            {
                Console.WriteLine("mangga lebih sedikit");
            }
        }

        static void optPenugasan()
        {
            int mangga = 10;
            int apel = 8;

            mangga = 15; // mengisi ulang variabel mangga

            Console.WriteLine("--Operator Penugasan--");
            Console.WriteLine($"Mangga = {mangga}");
            Console.WriteLine($"apel = {apel}");

            apel += mangga; 
            Console.WriteLine($"Apel + mangga = {apel}");

            mangga = 15;
            apel = 8;
            apel -= mangga; 
            Console.WriteLine($"Apel - mangga = {apel}");

            mangga = 15;
            apel = 8;
            apel *= mangga;
            Console.WriteLine($"Apel * mangga = {apel}");

            mangga = 15;
            apel = 8;
            apel /= mangga;
            Console.WriteLine($"Apel / mangga = {apel}");

            mangga = 15;
            apel = 8;
            apel %= mangga;
            Console.WriteLine($"Apel % mangga = {apel}");
        }

        static void modulus()
        {
            int mangga, orang, hasil = 0;
            Console.WriteLine("--Modulus--");
            Console.Write("jumlah mangga = ");
            mangga = int.Parse(Console.ReadLine());
            Console.Write("jumlah orang = ");
            orang = int.Parse(Console.ReadLine());

            hasil = mangga % orang; //operator modulus (sisa bagi)

            Console.WriteLine("Hasil Modulus = {0}", hasil);
        }

        static void operatorAritmatika()
        {
            int mangga, apel, hasil = 0;
            Console.WriteLine("--Aritmatika--");
            Console.Write("Jumlah Mangga = ");
            mangga = Convert.ToInt32(Console.ReadLine());
            Console.Write("Jumlah Apel = ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga + apel;

            Console.WriteLine($"Hasil mangga + apel = {hasil}");
        }

        static void Konversi()
        {
            int umur = 19; // variabel integer
            string strUmur = umur.ToString(); // mengkonversi int ke string

            Console.WriteLine(strUmur);

            int myInt = 10;
            double myDouble = 5.25;
            bool myBool = false;

            string myString = Convert.ToString(myInt); //konversi int ke string
            double myConvDouble = Convert.ToDouble(myInt); // konvert int ke double
            int myConvInt = Convert.ToInt32(myDouble);
            string myConvString = Convert.ToString(myBool);

            Console.WriteLine("Convert int to string :" + myString);
            Console.WriteLine("Convert int to Double :" + myConvDouble);
            Console.WriteLine("Convert double to int :" + myConvInt);
            Console.WriteLine("Convert bool to String :" + myConvString);
        }
    }
}
