﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Logic15
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal1();
            //soal1_2();
            //soal2();
            //soal2_2();
            //soal3();
            //soal9();
            //soalKisiKisi1();
            //soalKisiKisi2();
            //soal6();
            //recursiveDESC(); //fungsi yg memanggil dirinya sendiri
            //rekursiveASC();
            //arrayTarget();
            //recursiveDigit();
            //fobinaciUp();


            Console.ReadKey();
        }

        static void fobinaciUp()
        {
            Console.Write("Masukkan Maksimal himpunan : ");
            int num = int.Parse(Console.ReadLine());

            int[] numArr = new int[num];
            int[] numEven = new int[num];
            int[] numOdd = new int[num];

            int n1 = 2, n2 = 1;
            double res1 = 0, res2 = 0, res3 = 0;

            for (int i = 0; i < num; i++)
            {
                if (i <= 1)
                {
                    numArr[i] = 1;
                }
                else
                {
                    numArr[i] = numArr[i - 2] + numArr[i - 1];
                }

                numEven[i] = n1;
                n1 += 2;

                numOdd[i] = n2;
                n2 += 2;
            }

            for (int i = 0; i < num; i++)
            {
                res1 += numArr[i];
                res2 += numEven[i];
                res3 += numOdd[i];
            }


            Console.WriteLine("Fibonaci : " + string.Join(" ", numArr) + $"\tTotal : {res1}" + $"\tRata-rata : {Math.Round(res1 / numArr.Length, 2)}");
            Console.WriteLine("Genap    : " + string.Join(" ", numEven) + $"\tTotal : {res2}" + $"\tRata-rata : {Math.Round(res2 / numEven.Length, 2)}");
            Console.WriteLine("Ganjil   : " + string.Join(" ", numOdd) + $"\tTotal : {res3}" + $"\tRata-rata : {Math.Round(res3 / numOdd.Length, 2)}");
        }

        static void recursiveDigit()
        {
            Console.Write("Masukkan deret angka : ");
            char[] deret = Console.ReadLine().ToCharArray();

            Console.Write("Masukkan angka perulangan : ");
            int ulang = int.Parse(Console.ReadLine());

            int[] numArr = deret.Select(c => Convert.ToInt32(c.ToString())).ToArray();
            int totalP = 0;


            for (int i = 0; i < numArr.Length; i++)
            {
                totalP += numArr[i];
            }

            totalP = totalP * ulang;

            char[] cek = new char[3];
            int[] numArr2 = new int[3];

        loop:
            if (totalP > 10)
            {
                cek = totalP.ToString().ToCharArray();
                numArr2 = cek.Select(c => Convert.ToInt32(c.ToString())).ToArray();
            }
            Console.WriteLine(cek);
            totalP = 0;
            for (int i = 0; i < numArr2.Length; i++)
            {
                totalP += numArr2[i];
            }

            if (totalP > 10)
            {
                goto loop;
            }

            Console.WriteLine(totalP);
        }
        static void arrayTarget()
        {
            Console.Write("Masukkan Nilai Taget : ");
            int target = int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.Write("Masukkan Nilai Array : ");
            int[] numArr = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int count = 0;
            //string tmp = default;

            for (int i = 0; i < numArr.Length; i++)
            {
                for (int j = 0; j < numArr.Length; j++)
                {
                    if (numArr[i] - numArr[j] == target)
                    {
                        count++;
                        //tmp += numArr[i];
                        //tmp += numArr[j];
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine($"Terdapat {count} pasang dalam array dengan selisih {target}");
        }

        static void rekursiveASC()
        {
            Console.WriteLine("===Fungsi Recrusive===");

            Console.Write("Masukkan input : ");
            int input = int.Parse(Console.ReadLine());
            int start = 1;
            //panggil fungsi
            perulanganASC(input, start);
        }

        static int perulanganASC(int input, int start)
        {
            if (start == input)
            {
                Console.WriteLine(start);
                return start;
            }

            Console.WriteLine(start);

            return perulanganASC(input, start + 1);
        }

        static void recursiveDESC()
        {
            Console.WriteLine("===Fungsi Recrusive===");

            Console.Write("Masukkan input : ");
            int input = int.Parse(Console.ReadLine());

            //panggil fungsi
            perulangan(input);

            

        }

        static int perulangan(int input)
        {
            if(input == 1)
            {
                Console.WriteLine(input);
                return input;
            }

            Console.WriteLine(input);

            return perulangan(input - 1);
        }

        static void soal6()
        {

            
            Console.WriteLine("===soal no 6 pangram");

            string alpabet = "abcdefghijklmnopqrstuvwxyz";
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine().ToLower();

            bool pangram = true;

            foreach(char charAlpabet in alpabet)
            {
                if (!kalimat.Contains(charAlpabet))
                {
                    pangram = false;
                    break;
                }
            }
            if (pangram)
            {
                Console.WriteLine("Kalimat ini adalah pangram");
            }
            else
            {
                Console.WriteLine("kalimat ini bukan pangram");
            }
        }

        static void soalKisiKisi2()
        {
            Console.WriteLine("===Soal Kisi-kisi 2===");

            Console.Write("Masukkan Bilangan : ");
            int bilangan = int.Parse(Console.ReadLine());

            bool prima = true;

            

            if (bilangan >= 2)
            {
                int tampung = 0;
                //for(int i = 2; i <= bilangan; i++)
                //{
                for (int j = 2; j < bilangan; j++)
                {
                    if (bilangan % j == 0)
                    {
                        tampung = j;
                        prima = false;
                        //break; //untuk print pembagi terkecil
                    }
                }
                if (prima)
                {
                    Console.WriteLine("1");
                }
                else
                {
                    Console.WriteLine($"{tampung}");
                }
                //prima = true;
                //}
            }
            else
            {
                Console.WriteLine("Inputan Bukan Bilangan Prima");
            }
        }

        static void soalKisiKisi1()
        {
            Console.WriteLine("===Soal Kisi-kisi 1===");

            //List<int> list = new List<int>(){ 1,2,3,4,5,6,7,8,9,10 };
            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            list.Add(6);
            list.Add(7);
            list.Add(8);
            list.Add(9);
            list.Add(10);



            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] % 2 == 0)
                {
                    list.RemoveAt(i);
                    
                }
            }
           Console.WriteLine(String.Join(", ", list));

        }

        static void soal9()
        {
            Console.WriteLine("===Soal No 9 Beli Pulsa===");

            Console.Write("Masukkan Nominal transaksi beli pulsa (Rp.): ");
            int pulsa = int.Parse(Console.ReadLine());

            int point1 = 0, point2 = 0;

            if(pulsa <= 10000)
            {
                Console.WriteLine($"0 + {point1} = {point1} Point");
            }
            else if(pulsa <= 30000)
            {
                point1 = (pulsa - 10000) / 1000;
                Console.WriteLine($"0 + {point1} = {point1} Point");
            }
            else
            {
                point1 = (30000 - 10000) / 1000;
                point2 = ((pulsa - 30000) / 1000) * 2;
                Console.WriteLine($"0 + {point1} + {point2} = {point1 + point2} Point");
            }

            
        }
        static void soal3()
        {
            Console.WriteLine("===Soal No 3 Keranjang Buah");

            Console.Write("Masukkan jumlah buah keranjang 1 : ");
            int krj1 = int.Parse(Console.ReadLine());

            Console.Write("Masukkan jumlah buah keranjang 2 : ");
            int krj2 = int.Parse(Console.ReadLine());

            Console.Write("Masukkan jumlah buah keranjang 3 : ");
            int krj3 = int.Parse(Console.ReadLine());

            Console.Write("keranjang nomor berapa yang di bawa? : ");
            int ambil = int.Parse(Console.ReadLine());

            int hasil = 0;

            switch (ambil)
            {
                case 1:
                    hasil = krj2 + krj3;
                    break;
                case 2:
                    hasil = krj1 + krj3;
                    break;
                case 3:
                    hasil = krj1 + krj2;
                    break;
                default:
                    Console.WriteLine("Error!!");
                    break;
            }
            Console.WriteLine($"Sisa Buah adalah : {hasil}");
        }

        static void soal2_2()
        {
            Console.WriteLine("===Soal No 2 Nomor Invoice===");

            Console.Write("Start : ");
            int start = int.Parse(Console.ReadLine());

            Console.Write("End : ");
            int end = int.Parse(Console.ReadLine());

            string initial = "XA-";

            DateTime date = DateTime.Now;

            for(int i = start; i <= end; i++)
            {
                Console.WriteLine($"{initial}{date.ToString("ddMMyyyy")}-{i.ToString().PadLeft(5,'0')}");
            }
        }

        static void soal2()
        {
            Console.WriteLine("===Soal No 2 Nomor Invoice===");

            Console.Write("Start : ");
            int start = int.Parse(Console.ReadLine());

            Console.Write("End : ");
            int end = int.Parse(Console.ReadLine());

            DateTime tanggal = new DateTime(2022, 08, 07);

            string noInvoice = "";

            if (start <= end)
            {
                for (int i = start; i <= end; i++)
                {
                    noInvoice = "XA-" + tanggal.Day.ToString().PadLeft(2, '0') + tanggal.Month.ToString().PadLeft(2, '0') + tanggal.Year + "-" + i.ToString("00000");
                    Console.WriteLine(noInvoice);
                }
            }
            else
            {
                Console.WriteLine("Errorr!!!");
            }
        }

        static void soal1_2()
        {
            Console.WriteLine("==Soal 1 menghitung jumlah kata==");

            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            int count = 0, index = 0;//index apalbila huruf pertamanya kecil

            foreach(char item in kalimat)
            {
                if (char.IsUpper(item) || index == 0)
                {
                    count++;
                }
                index++;
            }
            Console.WriteLine(count);
        }

        static void soal1()
        {
            Console.WriteLine("==Soal 1 menghitung jumlah kata==");

            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            char[] array = kalimat.ToCharArray();

            int hasil = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (i == 0 || char.IsUpper(array[i]))
                {
                    hasil++;
                }
            }
            Console.WriteLine("Jumlah Kata Adalah " + hasil);
        }
    }
}
