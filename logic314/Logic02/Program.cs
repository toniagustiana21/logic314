﻿using System;

namespace Logic02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //ifStatement();
            //elseStatement();
            //ifNested();
            //ternary();
            //switchCase();
            //tugas1();
            //tugas2();
            tugas3();

            Console.ReadKey();
        }

        static void tugas3()
        {
            Console.WriteLine("--Latihan 3--");

            Console.Write("Masukkan Nilai : ");
            int angka = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Pembagi : ");
            int pembagi = int.Parse(Console.ReadLine());

            int hasil = angka % pembagi;

            if (hasil == 0)
            {
                Console.WriteLine($"hasil dari {angka} % {pembagi} adalah : 0");
            }
            else
            {
                Console.WriteLine($"hasil dari {angka} % {pembagi} adalah : {hasil}");
            }
        }

        static void tugas1()
        {
            Console.WriteLine("--latihan 1--");

           

            Console.Write("Masukkan nilai jari - jari : ");
            double r = double.Parse(Console.ReadLine());

            double keliling = 2 * 3.14 * r;
            double luas = 3.14 * r * r;
            Console.WriteLine($"Luas Lingkaran = {luas}");
            Console.WriteLine($"Keliling Lingkaran = {keliling}");
        }

        static void tugas2()
        {
            Console.WriteLine("--Latihan 2--");

            Console.Write("masukkan nilai sisi : ");
            double s = double.Parse(Console.ReadLine());

            double luas = s * s;
            double keliling = 4 * s;
            Console.WriteLine($"Luas Persegi = {luas}");
            Console.WriteLine($"Keliling persegi = {keliling}");
        }

        static void switchCase()
        {
            Console.WriteLine("--Switch Case--");

            Console.Write("Pilih buah kesukaan anda (apel, mangga, pisang) : ");
            string input = Console.ReadLine().ToLower();

            switch (input)
            {
                case "apel":
                    Console.WriteLine("Anda memilih buah apel");
                    break;
                case "mangga":
                    Console.WriteLine("Anda memilih buah mangga");
                    break;
                case "pisang":
                    Console.WriteLine("Anda memilih buah pisang");
                    break;
                default:
                    Console.WriteLine("Anda memilih buah yang lain");
                    break;
            }
        }

        static void ternary()
        {
            Console.WriteLine("--Ternary--");

            int x, y;
            Console.Write("Masukkan nilai x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukan nilai y : ");
            y = int.Parse(Console.ReadLine());

            string hasilTernary = x > y ? "nilai x lebih besar dari nilai y" :
                x < y ? "nilai x lebih kecil dari nilai y" : "nilai x sama dengan nilai y";

            Console.WriteLine(hasilTernary);
        }

        static void ifNested()
        {
            Console.WriteLine("--If Nested--");
            Console.Write("masukkan nilai : ");
            int nilai = int.Parse(Console.ReadLine());
            int nilaiMax = 100;

            if(nilai >= 70 && nilai <= nilaiMax)
            {
                Console.WriteLine("Kamu Berhasil!");
                if(nilai == nilaiMax)
                {
                    Console.WriteLine("Kamu Keren!!");
                }
            }
            else if(nilai >= 0 && nilai < 70)
            {
                Console.WriteLine("Kamu GAGAL!!");
            }
            else
            {
                Console.WriteLine("Masukkan Angka yang benar");
            }
        }

        static void elseStatement()
        {
            int x, y;
            Console.WriteLine("--Else Statement--");

            Console.Write("Masukan Nilai x = ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai Y = ");
            y = int.Parse(Console.ReadLine());

            if(x > y)
            {
                Console.WriteLine("Nilai x lebih besar dari nilai y");
            }
            else if(x < y)
            {
                Console.WriteLine("nilai x lebih kecil dari nilai y");
            }
            else
            {
                Console.WriteLine("nilai x sama dengan nilai y");
            }
        }

        static void ifStatement()
        {
            Console.WriteLine("--If Statement--");

            int x, y;
            Console.Write("Masukkan Nilai X = ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Nilai Y = ");
            y = int.Parse(Console.ReadLine());

            if(x > y)
            {
                Console.WriteLine("Nilai X Lebih Besar dari Nilai Y");
            }
            if(y > x)
            {
                Console.WriteLine("Nilai Y Lebih Besar dari Nilai X");
            }
            if(x == y)
            {
                Console.WriteLine("Nilai X Sama Dengan Nilai Y");
            }
        }
    }
}
