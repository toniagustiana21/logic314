﻿using System;

namespace Logic04_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal1();
            //soal2();
            //soal3();
            //soal4();
            //soal5();
            //soal6();
            //soal7();
            //soal8();
            //soal8_2();
            //soal9();
            //soal10();
            //soal11();
            //soal11_2();
            //soal12();
            soal12_2();
            Console.ReadKey();
        }

        static void soal12_2()
        {
            Console.WriteLine("Soal 12 (For Nested)");
            Console.WriteLine(new string('=', 30));

            Console.WriteLine("Masukkan Input : ");
            int n = int.Parse(Console.ReadLine());
            //int deret1 = 0, deret2 = 1;
            //string tampung = "";

            for(int i = 1; i <= n; i++)
            {
                for(int j = 1; j <= n; j++)
                {
                    if(i == 1 || i == n)
                    {
                        if(i == n)
                        {
                            Console.Write(n - j + 1 + "\t");
                        }
                        else
                        {
                            Console.Write(j + "\t");
                        }
                    }
                    else
                    {
                        if(j==1 || j == n)
                        {
                            Console.Write("*\t");
                        }
                        else
                        {
                            Console.Write(" \t");
                        }
                    }
                }
                Console.WriteLine();
            }
        }

        static void soal12()
        {
            Console.WriteLine("Soal 12 (For Nested)");
            Console.WriteLine(new string('=', 30));

            Console.WriteLine("Masukkan Input : ");
            int n = int.Parse(Console.ReadLine());
            int deret1 = 0, deret2 = 1;
            string tampung = "";
            for(int i = 0; i < n; i++)
            {
                for(int j = 0; j < n; j++)
                {
                    if(i == 0)
                    {
                        tampung += deret1++ + " ";

                    }
                }
            }
            

        }

        static void soal11_2()
        {
            Console.WriteLine("Soal 11 (Belanja Lebaran)");
            Console.WriteLine(new string('=', 30));

            Console.Write("Masukkan Uang Anda : ");
            int uang = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Harga Baju : ");
            string[] hrgBaju = Console.ReadLine().Split(",");

            Console.Write("Masukkan Harga Celana : ");
            string[] hrgCelana = Console.ReadLine().Split(",");
            int[] totalHarga = new int[hrgBaju.Length];

            for(int i = 0; i < totalHarga.Length; i++)
            {
                totalHarga[i] = int.Parse(hrgBaju[i]) + int.Parse(hrgCelana[i]);

            }

            int maxBelanja = 0;

            for (int i = 0; i < totalHarga.Length; i++)
            {
                for (int j = 0; j < totalHarga.Length; j++)
                {
                    
                    if (totalHarga[i] > totalHarga[j])
                    {
                        maxBelanja = totalHarga[i];
                        totalHarga[i] = totalHarga[j];
                        totalHarga[j] = maxBelanja;
                    }
                }
            }
            for(int i = 0; i < totalHarga.Length; i++)
            {
                if (totalHarga[i] <= uang)
                {
                    maxBelanja = totalHarga[i];
                    break;
                }
            }
            Console.WriteLine($"Total Harga Barang yang Mampu dibeli adalah : {maxBelanja}");
            Console.WriteLine($"Sisa uang anda : {uang - maxBelanja}");
        }

        static void soal11()
        {
            Console.WriteLine("Soal 11 (Belanja Lebaran)");
            Console.WriteLine(new string('=', 30));

            Console.Write("Masukkan Uang Anda : ");
            int uang = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Harga Baju : ");
            int[] hrgBaju = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            Console.Write("Masukkan Harga Celana : ");
            int[] hrgCelana = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            int maxBelanja = 0;

            for(int i = 0; i < hrgBaju.Length; i++)
            {
                for(int j = 0; j < hrgCelana.Length; j++)
                {
                    int harga = hrgBaju[i] + hrgCelana[j];
                    if(harga <= uang && harga >= maxBelanja)
                    {
                        maxBelanja = harga;
                    }
                }
            }
            Console.WriteLine($"Total Harga Barang yang Mampu dibeli adalah : {maxBelanja}");
            Console.WriteLine($"Sisa uang anda : {uang - maxBelanja}");
        }

        static void soal10()
        {
            Console.WriteLine("Soal 10 (IMP Fashion)");
            Console.WriteLine(new string('=', 30));

            int kodeBaju, harga = 0;
            string kodeUK, merkBaju = "";

            Console.Write("Masukkan Kode Baju   : ");
            kodeBaju = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Kode Ukuran : ");
            kodeUK = Console.ReadLine().ToUpper();

            if(kodeBaju >= 1 && kodeBaju <= 3)
            {
                if(kodeBaju == 1)
                {
                    merkBaju = "IMP";
                    if(kodeUK == "S")
                    {
                        harga = 200000;
                    }
                    else if(kodeUK == "M")
                    {
                        harga = 220000;
                    }
                    else
                    {
                        harga = 250000;
                    }
                }
                if (kodeBaju == 2)
                {
                    merkBaju = "Prada";
                    if (kodeUK == "S")
                    {
                        harga = 150000;
                    }
                    else if (kodeUK == "M")
                    {
                        harga = 160000;
                    }
                    else
                    {
                        harga = 170000;
                    }
                }
                if (kodeBaju == 3)
                {
                    merkBaju = "Gucci";
                    if (kodeUK == "S" || kodeUK == "M")
                    {
                        harga = 200000;
                    }
                    else
                    {
                        harga = 200000;
                    }
                }
                Console.WriteLine(new string('=', 30));
                Console.WriteLine($"Merk Baju   : {merkBaju}");
                Console.WriteLine($"Harga       : {harga}");

            }
            else
            {
                Console.WriteLine("Kode Baju yang Anda Masukkan Salah");
            }
            
        }

        static void soal9_2()
        {
            Console.Write("Masukkan format waktu : ");
            string formatJam = Console.ReadLine().ToUpper();

            string pm = formatJam.Substring(8, 2);
            int jam = int.Parse(formatJam.Substring(0, 2));

            if(jam <= 12)
            {
                if(pm == "PM")
                {
                    jam += 12;
                    Console.WriteLine(jam.ToString() + formatJam.Substring(2, 6));
                }
                else
                {
                    Console.WriteLine(formatJam.Substring(0, 8));
                }
            }
            else
            {
                Console.WriteLine("Masukkan Data yang benar");
            }
        }

        static void soal9()
        {
            Console.WriteLine("Soal 9 (Time Conversion)");
            Console.WriteLine(new string('=', 30));

            Console.Write("Masukkan Waktu : ");
            string waktu = Console.ReadLine();

            int jam24 = 12;
            int pm = 0;
            string PM = waktu.Substring(8, 2);
            int hasil = 0;
            string jam = "";
            jam = waktu.Substring(0, 2);
            if (PM == "PM")
            {
                pm += jam24;
                
                hasil = int.Parse(jam) + pm;

                Console.WriteLine($"{hasil}{waktu.Substring(2, 6)}");
            }
            else if(PM == "AM")
            {
                Console.WriteLine($"{waktu.Substring(0, 8)}");
            }
            else
            {
                Console.WriteLine("Jam ERROR!");
            }
            


        }

        static void soal8_2()
        {
            Console.WriteLine("Soal 8 (Fibonacci)");
            Console.WriteLine(new string('=', 30));

            int number;

            Console.Write("Masukkan Panjang Deret : ");
            number = int.Parse(Console.ReadLine());

            int[] numberArray = new int[number];

            for(int i = 0; i < number; i++)
            {
                if(i <= 1)
                {
                    numberArray[i] = 1;
                }
                else
                {
                    numberArray[i] = numberArray[i - 2] + numberArray[i - 1];
                }
            }
            Console.Write(string.Join(" ", numberArray));
        }

        static void soal8()
        {
            Console.WriteLine("Soal 8 (Fibonacci)");
            Console.WriteLine(new string('=', 30));

            int number, angkaN = 1, angkaB = 1, hasil = 0;

            Console.Write("Masukkan Panjang Deret : ");
            number = int.Parse(Console.ReadLine());

            for (int i = 0; i < number; i++)
            {
                if(i <= 1)
                {
                    hasil = 1;
                }
                else
                {
                    hasil = angkaN + angkaB;
                    angkaN = angkaB;
                    angkaB = hasil;
                }
                

                Console.Write($"{hasil} ");
            }
            

        }

        static void soal7()
        {
            Console.WriteLine("Soal 7 (Looping)");
            Console.WriteLine(new string('=', 30));

            int n, angka;

            Console.Write("Masukkan Panjang Deret : ");
            n = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Angka perkalian : ");
            angka = int.Parse(Console.ReadLine());

            for(int i = 1; i <= n; i++)
            {
                
                if(i % 2 == 0)
                {
                    Console.Write($"{angka * i *(-1)} ");
                }
                else
                {
                    Console.Write($"{angka*i} ");
                }
                
            }
        }

        static void soal6()
        {
            Console.WriteLine("Soal 6 (Looping)");
            Console.WriteLine(new string('=', 30));

            int angka = 3, n;

            Console.Write("Masukkan Panjang Deret : ");
            n = int.Parse(Console.ReadLine());

            for(int i = 1; i <= n; i++)
            {
                if(i % 2 == 0)
                {
                    Console.Write("*");
                }
                else
                {
                    Console.Write(angka);
                }
                angka *= 3;
                Console.Write("\t");

               
            }
        }

        static void soal5()
        {
            Console.WriteLine("Soal 5 (Manipulasi String)");
            Console.WriteLine(new string('=', 30));

            Console.Write("Masukkan Kalimat : ");
            string[] input = Console.ReadLine().Split(" ");

            string tampung = "";

            for(int i = 0; i < input.Length; i++)
            {
                char[] chars = input[i].ToCharArray();
                for(int j = 0; j < chars.Length; j++)
                {
                    if(j > 0)
                    {
                        tampung += chars[j];
                        if(j == chars.Length - 1)
                        {
                            tampung += " ";
                        }
                    }
                   
                }
                //tampung += " ";
            }
            Console.WriteLine(tampung);
            
        }

        static void soal4()
        {
            Console.WriteLine("Soal 4 (Manipulasi String)");
            Console.WriteLine(new string('=', 30));

            Console.Write("Masukkan Kalimat : ");
            string[] input = Console.ReadLine().Split(" ");
          
            

            for(int i = 0; i < input.Length; i++)
            {
               
                for(int j = 0; j < input[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(input[i].Replace(input[i], "*"));
                    }
                    else if (j == input[i].Length - 1)
                    {
                        Console.Write(input[i].Replace(input[i], "*"));
                    }
                    else
                    {
                        Console.Write(input[i][j]);
                    }
                }
                Console.Write(" ");
            }
           
        }

        static void soal3()
        {
            Console.WriteLine("Soal 3 (Manipulasi String)");
            Console.WriteLine(new string('=', 30));

            Console.Write("Masukkan Kalimat : ");
            string input = Console.ReadLine();
            string[] kataArray = input.Split(" ");
            string output = "";

            for(int i = 0; i < kataArray.Length; i++)
            {
                if (kataArray[i] != "")
                {
                    string kata = kataArray[i];
                    string pertama = kata.Substring(0, 1);
                    string akhir = kata.Substring(kata.Length - 1);
                    string tengah = kata.Substring(1, kata.Length - 2);
                    tengah = tengah.Replace(tengah, new string('*', tengah.Length));

                    string kataBaru = pertama + tengah + akhir;

                    output += kataBaru + " ";
                }
            }
            Console.WriteLine($"Hasil : {output}");
        }

        static void soal2()
        {
            Console.WriteLine("Soal 2 (Split & Join)");
            Console.WriteLine(new string('=', 30));

            int n = 0;
            Console.Write("Masukkan Kalimat : ");
            string[] kalimat = Console.ReadLine().Split(" ");
            //string[] kataArray = kalimat.Split(" ");
            foreach(string kata in kalimat)
            {
                n++;
                Console.WriteLine($"Kata {n} : {kata}");
            }
            Console.WriteLine($"Jumlah Kata Adalah {n}");
        }

        static void soal1()
        {
            ulangi:
            Console.WriteLine("Soal 1 (Gaji Karyawan)");
            Console.WriteLine(new string('=', 30));

            int gol1 = 2000, gol2 = 3000, gol3 = 4000, gol4 = 5000;
            double jamKerja, golKaryawan, upah = 0, totGaji = 0 , lembur = 0;

            Console.Write("Masukkan Golongan Karyawan   : ");
            golKaryawan = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Jam Kerja           : ");
            jamKerja = int.Parse(Console.ReadLine());

            double lemburPerJam = 1.5;
            
            
            if(golKaryawan == 1 && jamKerja >= 1)
            {
                if(jamKerja >= 1 && jamKerja <= 40)
                {
                    totGaji = upah = gol1 * jamKerja;
                }
                else
                {
                    jamKerja -= 40;
                    upah = gol1 * 40;
                    lembur = gol1 * jamKerja * lemburPerJam;
                    totGaji = upah + lembur;
                }
                
                
            }
            else if(golKaryawan == 2 && jamKerja >= 1)
            {
                if(jamKerja >= 1 && jamKerja <= 40)
                {
                    totGaji = upah = gol2 * jamKerja;
                }
                else
                {
                    jamKerja -= 40;
                    upah = gol2 * 40;
                    lembur = gol2 * jamKerja * lemburPerJam;
                    totGaji = upah + lembur;
                }
            }
            else if(golKaryawan == 3 && jamKerja >= 1)
            {
                if(jamKerja >= 1 && jamKerja <= 40)
                {
                    totGaji = upah = gol3 * jamKerja;
                }
                else
                {
                    jamKerja -= 40;
                    upah = gol3 * 40;
                    lembur = gol3 * jamKerja * lemburPerJam;
                    totGaji = upah + lembur;
                }
            }
            else if(golKaryawan == 4 && jamKerja >= 1)
            {
                if(jamKerja >= 1 && jamKerja <= 40)
                {
                    totGaji = upah = gol4 * jamKerja;
                }
                else
                {
                    jamKerja -= 40;
                    upah = gol4 * 40;
                    lembur = gol4 * jamKerja * lemburPerJam;
                    totGaji = upah + lembur;
                }
            }
            else
            {
                Console.WriteLine("Data Yang Anda Masukkan Salah");
                Console.WriteLine("Ulangi Proses ? (y/n)");
                string input = Console.ReadLine().ToLower();
                if (input == "y")
                {
                    goto ulangi;
                }

            }
            Console.WriteLine($"Upah        : {upah}");
            Console.WriteLine($"Lembur      : {lembur}");
            Console.WriteLine($"Total Gaji  : {totGaji}");

            
            
        }

        
    }
}
