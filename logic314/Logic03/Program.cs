﻿using System;

namespace Logic03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //tugas1();
            //tugas2();
            //tugas3();
            //tugas4();
            //perulanganWhile();
            //perulanganDoWhile();
            //perulanganFor();
            //Break();
            //Continue();
            //forBersarang();
            //arrayStatic();
            //arrayForeach();
            //arrayFor();
            //array2Dimensi();
            //array2Dimensifor();
            //splitJoin();
            //subString();
            //stringToCharArray();

            Console.ReadKey();
        }

        static void stringToCharArray()
        {
            Console.WriteLine("--String To Char Array--");
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            char[] array = kalimat.ToCharArray();

            foreach(char ch in array)
            {
                Console.WriteLine(ch);
            }
        }

        static void subString()
        {
            Console.WriteLine("--Sub String--");

            Console.Write("masukkan kalimat : ");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Substring (1,4) : " + kalimat.Substring(1, 4));
            Console.WriteLine("Substring (5,2) : " + kalimat.Substring(5, 2));
            Console.WriteLine("Substring (7,9) : " + kalimat.Substring(7, 9));
            Console.WriteLine("Substring (9) : " + kalimat.Substring(9));
           

        }

        static void splitJoin()
        {
            Console.WriteLine("--Split & Join--");
            Console.Write("Masukkan kalimat : ");
            string kalimat = Console.ReadLine();
            string[] kataArray = kalimat.Split(" ");
            foreach(string kata in kataArray)
            {
                Console.WriteLine(kata);
            }

            Console.WriteLine(string.Join(",", kataArray));
        }

        static void array2Dimensifor()
        {
            int[,] array = new int[,]
            {
                { 1,2,3 },
                { 4,5,6 },
                { 7,8,9 }
            };
            //cetak menggunkan for
            for(int i = 0;  i < 3; i++)
            {
                for(int j = 0; j < 3; j++)
                {
                    Console.Write(array[i,j] + " ");
                }
                Console.WriteLine();
            }
        }

        static void array2Dimensi()
        {
            int[,] array = new int[,]
            {
                { 1,2,3 },
                { 4,5,6 }, 
                { 7,8,9 }
            };

            //cetak
            Console.WriteLine(array[0,1]);
        }

        static void arrayFor()
        {
            string[] array = new string[]
           {
                "Abdullah Muafa",
                "Isni Dwitiniartdi",
                "Marcelino",
                "Ilham Rizki",
                "Alwi Fadli Siregar"
           };

            //baca array menggunakan foreach
            for(int i =0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
            
        }

        static void arrayForeach()
        {
            string[] array = new string[]
            {
                "Abdullah Muafa",
                "Isni Dwitiniartdi",
                "Marcelino",
                "Ilham Rizki",
                "Alwi Fadli Siregar"
            };

            //baca array menggunakan foreach
            foreach (string item in array)
            {
                Console.WriteLine(item);
            }

        }

        static void arrayStatic()
        {
            Console.WriteLine("--Array--");

            int[] staticIntArray = new int[3];

            //isi array
            staticIntArray[0] = 1;
            staticIntArray[1] = 2;
            staticIntArray[2] = 3;

            Console.WriteLine(staticIntArray[0]);
            Console.WriteLine(staticIntArray[1]);
            Console.WriteLine(staticIntArray[2]);
        }

        static void forBersarang()
        {
            for(int i = 0; i < 3; i++)
            {
                for(int j = 0; j < 3; j++)
                {
                    Console.Write($"[{i},{j}]");
                }
                //Console.WriteLine();
                Console.Write("\n");
            }
        }

        static void Continue()
        {
            for (int i = 0; i < 10; i++)
            {
                if(i == 5)
                {
                    continue;
                }
                Console.WriteLine(i);
            }
        }

        static void Break()
        {
            for(int i = 0; i < 10; i++)
            {
                if(i == 5)
                {
                    break;
                }
                Console.WriteLine(i);
            }
        }

        static void perulanganForDec()
        {
            for(int i = 10; i > 0; i--)
            {
                Console.Write("({0})", i);
            }
        }

        static void perulanganFor()
        {
            for(int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }
        }

        static void perulanganDoWhile()
        {
            int a = 0;
            do
            {
                Console.WriteLine(a);
                a++;
            } while (a < 5);
        }

        static void perulanganWhile()
        {

            Console.WriteLine("--Perulangan While--");
            bool ulangi = true;
            int nilai = 1;
            while (ulangi)
            {
                Console.WriteLine($"proses ke : {nilai}");
                

                Console.Write("apakah anda akan mengulangi proses? (y/n)");
                string input = Console.ReadLine().ToLower();
                if(input == "n")
                {
                    Console.WriteLine("akhir dari perulangan");
                    ulangi = false;
                   
                }
                else if(input == "y")
                {
                    ulangi = true;
                    nilai++;
                }
                else
                {
                    nilai = 1;
                    Console.WriteLine("ERROR!!");
                    //Console.WriteLine("apakah anda akan mengulangi proses? (y/n)");
                }
            }
        }

        static void tugas4()
        {
            Console.WriteLine("--Latihan soal 4--");

            Console.Write("Masukkan Angka = ");
            int input = int.Parse(Console.ReadLine());

            int hasil = input % 2;

            if(hasil == 0)
            {
                Console.WriteLine($"angka {input} adalah bilangan genap");
            }
            else
            {
                Console.WriteLine($"angka {input} adalah bilangan ganjil");
            }
        }

        static void tugas3()
        {
            Console.WriteLine("--Latihan Soal 3--");

            Console.Write("Masukkan nilai anda : ");
            int nilai = int.Parse(Console.ReadLine());

            if(nilai >= 80 && nilai <= 100)
            {
                Console.WriteLine("Anda mendapatpkan grade A");
            }
            else if(nilai >= 60 && nilai < 80)
            {
                Console.WriteLine("Anda mendapatkan grade B");
            }
            else if(nilai >= 0 && nilai < 60)
            {
                Console.WriteLine("Anda mendapatkan grade C");
            }
            else
            {
                Console.WriteLine("Masukkan nilai yang benar");
            }
        }

        static void tugas2()
        {
            Console.WriteLine("--Latihan Soal 2--");

            Console.Write("Puntung Rokok = ");
            int puntungRokok = int.Parse(Console.ReadLine());

            int sisa = puntungRokok % 8;
            int rBatang = puntungRokok / 8;
            int hrg1bt = 500;

            if(rBatang > 0)
            {
                int hargaTot = rBatang * hrg1bt;
                Console.WriteLine($"berhasil merangkai rokok sebanyak {rBatang}");
                Console.WriteLine($"dapat dijual dengan mendapatkan penghasilan sebesar Rp.{hargaTot}");
                if(sisa > 0)
                {
                    Console.WriteLine($"Sisa puntung Rokok adalah {sisa}");
                }
            }
            else
            {
                Console.WriteLine("tidak dapat menjual dan merangkai rokok");
            }


        }

        static void tugas1()
        {
            Console.WriteLine("--Latihan soal 1--");

            Console.Write("Masukkan Nilai : ");
            int angka = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Pembagi : ");
            int pembagi = int.Parse(Console.ReadLine());

            int hasil = angka % pembagi;

            Console.WriteLine($"Hasil dari {angka} % {pembagi} adalah : {hasil}");
        }
    }
}
