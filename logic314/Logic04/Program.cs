﻿using System;

namespace Logic04
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //removeString();
            //insertString();
            replaceString();

            Console.ReadKey();
        }

        static void replaceString()
        {
            Console.WriteLine("--Replace String--");

            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            Console.Write("Dari Kata : ");
            string kataLama = Console.ReadLine();

            Console.Write("Replace Menjadi Kata : ");
            string kataBaru = Console.ReadLine();

            Console.WriteLine($"Hasil Replace String : {kalimat.Replace(kataLama, kataBaru)}");
        }

        static void insertString()
        {
            Console.WriteLine("--Insert String--");

            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            Console.Write("Masukkan kata yang mau di insert : ");
            string input = Console.ReadLine();

            Console.Write("Masukkan Parameter : ");
            int param = int.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil Insert String : {kalimat.Insert(param, input)}");
        }

        static void removeString()
        {
            Console.WriteLine("--Remove String--");

            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            Console.Write("Isi Parameter Remove : ");
            int param = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine($"Hasil Remove String : {kalimat.Remove(param)}");
        }
    }
}
