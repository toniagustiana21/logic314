﻿using System;

namespace soalKisiKisi
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //bilPrima();
            //bilPrima2();
            //soal1();
            //soal2();
            //soal3();
            soal6();

            Console.ReadKey();
        }

        static void soal6()
        {
            Console.WriteLine("===Soal No 6 Panagram");

            Console.Write("Masukkan Kalimat : ");
            string[] kalimat = Console.ReadLine().ToLower().Split(' ');

            string alphabet = "abcdefghijklmnopqrstuvwxyz";
            char[] arrayAlpha = alphabet.ToCharArray();

            int count = 0;
            string tampung = "";

            for(int i = 0; i < kalimat.Length; i++)
            {
                for (int j = 0; j < arrayAlpha[i]; j++)
                {
                    if (kalimat[i].Contains(arrayAlpha[i]))
                    {
                        tampung = "Kalimat ini mengandung paragman";
                    }
                    else
                    {
                        tampung = "kalimat ini tidak mengandung paragman";
                    }
                } 
            }
            Console.WriteLine(tampung);
        }

        static void soal1()
        {
            Console.WriteLine("==Soal 1 menghitung jumlah kata==");

            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();

            char[] array = kalimat.ToCharArray();

            int hasil = 0;

            for(int i = 0; i < array.Length; i++)
            {
                if(i == 0 || char.IsUpper(array[i]))
                {
                    hasil++;
                }
            }
            Console.WriteLine("Jumlah Kata Adalah " + hasil);
        }

        static void soal3()
        {
            Console.WriteLine("===Soal No 3 Keranjang Buah===");

            Console.Write("keranjang 1 : ");
            int n = int.Parse(Console.ReadLine());


            Console.Write("keranjang 2 : ");
            int m = int.Parse(Console.ReadLine());

            Console.WriteLine("Keranjang 3 : kosong");
            //int o = int.Parse(Console.ReadLine());

            Console.Write("Keranjang yang Diambil (1-3) : ");
            int ambil = int.Parse(Console.ReadLine());

            int kerKosong = 0;
            int totalBuah = 0;

            if(ambil > 0 && ambil <= 3)
            {
                if(ambil == 1)
                {
                    totalBuah = m + kerKosong;
                }
                else if(ambil == 2)
                {
                    totalBuah = n + kerKosong;
                }
                else
                {
                    totalBuah = n + m;
                }
            }
            Console.WriteLine("Sisa Buah " + totalBuah);
        }

        static void soal2()
        {
            Console.WriteLine("===Soal No 2 Nomor Invoice===");

            Console.Write("Start : ");
            int start = int.Parse(Console.ReadLine());

            Console.Write("End : ");
            int end = int.Parse(Console.ReadLine());

            DateTime tanggal = new DateTime(2022, 08, 07);

            string noInvoice = "";

            if (start <= end)
            {
                for (int i = start; i <= end; i++)
                {
                    noInvoice = "XA-" + tanggal.Day.ToString().PadLeft(2, '0') + tanggal.Month.ToString().PadLeft(2, '0') + tanggal.Year + "-" + i.ToString("00000");
                    Console.WriteLine(noInvoice);
                }
            }
            else
            {
                Console.WriteLine("Errorr!!!");
            }
            
            
        }

        static void bilPrima2()
        {
            Console.WriteLine("===Bilangan Prima===");

            Console.Write("Masukkan angka : ");
            int bilangan = int.Parse(Console.ReadLine());

            bool prima = true;
            if (bilangan >= 2)
            {
                for (int i = 2; i <= bilangan; i++)
                {
                    for (int j = 2; j < i; j++)
                    {
                        if ((i % j) == 0)
                        {
                            prima = false;
                            Console.WriteLine(i + " Bukan Bilangan Prima");
                            break;
                        }
                    }
                    if (prima)
                        Console.WriteLine(i + " Adalah Bilangan Prima");
                    prima = true;
                }
            }
            else
            {
                Console.WriteLine("Tidak Ada Bilangan Prima");
                Console.WriteLine();
            }
        }
        static void bilPrima()
        {
            Console.WriteLine("===Bilangan Prima===");

            int bil, sisa, jmlBilangan = 0;
            string ket = "";

            Console.Write("Masukkan Input : ");
            bil = int.Parse(Console.ReadLine());

            for(int i = 1; i <= bil; i++)
            {
                sisa = bil % i;
                if(sisa == 0)
                {
                    jmlBilangan++;
                }
                else
                {
                    jmlBilangan = jmlBilangan;
                }
            }
            if (jmlBilangan > 2)
            {
                ket += "Bukan Bilangan Prima";
            }
            else
            {
                ket += "Bilangan Prima";
            }
            Console.WriteLine($"{bil} : {ket}");
        }
    }
}
