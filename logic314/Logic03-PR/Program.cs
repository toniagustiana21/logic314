﻿using System;

namespace Logic03_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal1();
            //soal2();
            //soal3();
            //soal4();
            //soal5();
            //soal6();
            //Soal7();
            Soal8();

            Console.ReadKey();
        }

        static void Soal8()
        {
            Console.WriteLine(new string('=', 50));
            Console.WriteLine(new string(' ', 13) + "Soal 8 (Hitung Rata-rata)");
            Console.WriteLine(new string('=', 50));

            double mtk, fisika, kimia;
            double rataRata = 0;

            Console.Write("Masukan Nilai MTK    : ");
            mtk = double.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai Fisika : ");
            fisika = double.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai Kimia  : ");
            kimia = double.Parse(Console.ReadLine());

            rataRata = (mtk + fisika + kimia) / 3;
            Console.WriteLine($"Nilai Rata-rata : {Math.Round(rataRata, 3)}");

            string ternary = rataRata < 50 ? "Maaf\nKamu Gagal" : "Selamat\nKamu Berhasil\nKamu Hebat";
            Console.WriteLine(ternary);

            //if (rataRata < 50)
            //{
            //    Console.Write("Maaf\nKamu Gagal");
            //}
            //else
            //{
            //    Console.Write("Selamat\nKamu Berhasil\nKamu Hebat");
            //}
        }

        static void Soal7()
        {
        ulang:
            
            Console.WriteLine("--Soal 7--");

            double beratBadan, tinggiBadan;
            double BMI = 0;
            double ubahCmKeM = 0;
            double meterKuadtrat = 0;
            string status;

            Console.Write("Masukkan berat badan anda (kg) : ");
            beratBadan = int.Parse(Console.ReadLine());
            Console.Write("Masukkan tinggi badan anda (cm) : ");
            tinggiBadan = int.Parse(Console.ReadLine());

            ubahCmKeM = tinggiBadan / 100;
            meterKuadtrat = Math.Pow(ubahCmKeM, 2);
            BMI = beratBadan / meterKuadtrat;
            Console.WriteLine($"Nilai BMI anda adalah : {Math.Round(BMI, 3)}");

            if (BMI >= 0 && BMI < 18.5)
            {
                status = "Terlalu Kurus";
            }
            else if (BMI >= 18.5 && BMI < 25.5)
            {
                status = "langsing";
                
            }
            else if (BMI >= 25.5)
            {
                status = "Gemuk";
               
            }
            else
            {
                Console.WriteLine("ERROR !, Masukkan data yang benar");
                goto ulang;
            }
            Console.WriteLine($"Anda Termasuk Berbadan {status}");
        }

        static void soal6()
        {
            Console.WriteLine("--Soal 6--");

            string nama;
            int tunjangan, gapok, banyakBulan;

            Console.Write("Masukkan Nama Anda : ");
            nama = Console.ReadLine();
            Console.Write("Tunjangan : ");
            tunjangan = int.Parse(Console.ReadLine());
            Console.Write("Gaji Pokok : ");
            gapok = int.Parse(Console.ReadLine());
            Console.Write("Banyak Bulan : ");
            banyakBulan = int.Parse(Console.ReadLine());

            int gajiPlusTunjangan = gapok + tunjangan;
            double bpjs = 0.03 * gajiPlusTunjangan;
            double pajak, pajakPlusBpjs, gajiPerBulan, totGaji;

            if (gajiPlusTunjangan >= 0 && gajiPlusTunjangan <= 5000000)
            {
                pajak = 0.05 * gajiPlusTunjangan;
                pajakPlusBpjs = pajak + bpjs;
                gajiPerBulan = gajiPlusTunjangan - pajakPlusBpjs;
                totGaji = gajiPerBulan * banyakBulan;

                Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut : ");
                Console.WriteLine($"Pajak                   : Rp.{pajak}");
                Console.WriteLine($"bpjs                    : Rp.{bpjs}");
                Console.WriteLine($"gaji/bulan              : Rp.{gajiPerBulan}");
                Console.WriteLine($"Total gaji/banyak bulan : Rp.{totGaji}");
            }
            else if (gajiPlusTunjangan > 5000000 && gajiPlusTunjangan <= 10000000)
            {
                pajak = 0.1 * gajiPlusTunjangan;
                pajakPlusBpjs = pajak + bpjs;
                gajiPerBulan = gajiPlusTunjangan - pajakPlusBpjs;
                totGaji = gajiPerBulan * banyakBulan;

                Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut : ");
                Console.WriteLine($"Pajak                   : Rp.{pajak}");
                Console.WriteLine($"bpjs                    : Rp.{bpjs}");
                Console.WriteLine($"gaji/bulan              : Rp.{gajiPerBulan}");
                Console.WriteLine($"Total gaji/banyak bulan : Rp.{totGaji}");
            }
            else if (gajiPlusTunjangan > 10000000)
            {
                pajak = 0.15 * gajiPlusTunjangan;
                pajakPlusBpjs = pajak + bpjs;
                gajiPerBulan = gajiPlusTunjangan - pajakPlusBpjs;
                totGaji = gajiPerBulan * banyakBulan;

                Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut : ");
                Console.WriteLine($"Pajak                   : Rp.{pajak}");
                Console.WriteLine($"bpjs                    : Rp.{bpjs}");
                Console.WriteLine($"gaji/bulan              : Rp.{gajiPerBulan}");
                Console.WriteLine($"Total gaji/banyak bulan : Rp.{totGaji}");
            }
            else
            {
                Console.WriteLine("Masukkan Data Yang Benar !");
            }
        }

        static void soal5()
        {
            Console.WriteLine("--Soal 5");

            Console.Write("Masukkan Nama Anda : ");
            string nama = Console.ReadLine();
            Console.Write("Tahun Berapa Anda Lahir? : ");
            int umur = int.Parse(Console.ReadLine());

            if (umur >= 1944 && umur <= 1964)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Baby Boomer");
            }
            else if (umur >= 1965 && umur <= 1979)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi X");
            }
            else if (umur >= 1980 && umur <= 1994)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Y");
            }
            else if (umur >= 1995)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Z");
            }
            else
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tidak tergolong apapun");
            }
        }

        static void soal4()
        {
            Console.WriteLine("--soal 4--");

            int belanja, ongkosKirim, voucher;

            Console.Write("Belanja : ");
            belanja = int.Parse(Console.ReadLine());
            Console.Write("Ongkos Kirim : ");
            ongkosKirim = int.Parse(Console.ReadLine());
            Console.Write("Kode Voucher : ");
            voucher = int.Parse(Console.ReadLine());

            int minBelanja30rb = 5000, ongkir30rb = 5000;
            int minBelanja50rb = 10000, ongkir50rb = 10000;
            int minBelanja100rb = 10000, ongkir100rb = 20000;

            int harga;

            if(ongkosKirim > 0)
            {
                if (belanja >= 30000 && belanja < 50000)
                {

                    if (voucher == 1)
                    {
                        Console.WriteLine("Berhasil Memasukkan Kode Voucher 1 (Minimal Order 30rb)");
                        harga = (belanja + ongkosKirim) - (minBelanja30rb + ongkir30rb);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : {ongkir30rb}");
                        Console.WriteLine($"Diskon Belanja  : {minBelanja30rb}");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                    else if (voucher == 2)
                    {
                        Console.WriteLine("Gagal Memasukkan Kode Voucher 2 (Minimal Order 50rb)");
                        harga = (belanja + ongkosKirim);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : 0");
                        Console.WriteLine($"Diskon Belanja  : 0");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                    else if (voucher == 3)
                    {
                        Console.WriteLine("Gagal Memasukkan Kode Voucher 3 (Minimal Order 100rb)");
                        harga = (belanja + ongkosKirim);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : 0");
                        Console.WriteLine($"Diskon Belanja  : 0");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                    else
                    {
                        Console.WriteLine("Gagal Memasukkan Kode Voucher (Kode voucher tidak ditemukan)");
                        harga = (belanja + ongkosKirim);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : 0");
                        Console.WriteLine($"Diskon Belanja  : 0");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                }
                else if (belanja >= 50000 && belanja < 100000)
                {
                    if (voucher == 1)
                    {
                        Console.WriteLine("Berhasil Memasukkan Kode Voucher 1 (Minimal Order 30rb)");
                        harga = (belanja + ongkosKirim) - (minBelanja30rb + ongkir30rb);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : {ongkir30rb}");
                        Console.WriteLine($"Diskon Belanja  : {minBelanja30rb}");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                    else if (voucher == 2)
                    {
                        Console.WriteLine("Berhasil Memasukkan Kode Voucher 2 (Minimal Order 50rb)");
                        harga = (belanja + ongkosKirim) - (minBelanja50rb + ongkir30rb);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : {ongkir50rb}");
                        Console.WriteLine($"Diskon Belanja  : {minBelanja50rb}");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                    else if (voucher == 3)
                    {
                        Console.WriteLine("Gagal Memasukkan Kode Voucher 3 (Minimal Order 100rb)");
                        harga = (belanja + ongkosKirim);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : 0");
                        Console.WriteLine($"Diskon Belanja  : 0");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                    else
                    {
                        Console.WriteLine("Gagal Memasukkan Kode Voucher (Kode voucher tidak ditemukan)");
                        harga = (belanja + ongkosKirim);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : 0");
                        Console.WriteLine($"Diskon Belanja  : 0");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                }
                else if (belanja >= 100000)
                {
                    if (voucher == 1)
                    {
                        Console.WriteLine("Berhasil Memasukkan Kode Voucher 1 (Minimal Order 30rb)");
                        harga = (belanja + ongkosKirim) - (minBelanja30rb + ongkir30rb);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : {ongkir30rb}");
                        Console.WriteLine($"Diskon Belanja  : {minBelanja30rb}");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                    else if (voucher == 2)
                    {
                        Console.WriteLine("Berhasil Memasukkan Kode Voucher 2 (Minimal Order 50rb)");
                        harga = (belanja + ongkosKirim) - (minBelanja50rb + ongkir30rb);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : {ongkir50rb}");
                        Console.WriteLine($"Diskon Belanja  : {minBelanja50rb}");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                    else if (voucher == 3)
                    {
                        Console.WriteLine("Berhasil Memasukkan Kode Voucher 3 (Minimal Order 100rb)");
                        harga = (belanja + ongkosKirim) - (minBelanja100rb + ongkir100rb);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : {ongkir100rb}");
                        Console.WriteLine($"Diskon Belanja  : {minBelanja100rb}");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                    else
                    {
                        Console.WriteLine("Gagal Memasukkan Kode Voucher (Kode voucher tidak ditemukan)");
                        harga = (belanja + ongkosKirim);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : 0");
                        Console.WriteLine($"Diskon Belanja  : 0");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                }
                else if (belanja > 0 && belanja < 30000)
                {
                    if (voucher == 1)
                    {
                        Console.WriteLine("Gagal Memasukkan Kode Voucher 1 (Minimal Order 30rb)");
                        harga = (belanja + ongkosKirim);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : 0");
                        Console.WriteLine($"Diskon Belanja  : 0");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                    else if (voucher == 2)
                    {
                        Console.WriteLine("Gagal Memasukkan Kode Voucher 2 (Minimal Order 50rb)");
                        harga = (belanja + ongkosKirim);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : 0");
                        Console.WriteLine($"Diskon Belanja  : 0");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                    else if (voucher == 3)
                    {
                        Console.WriteLine("Gagal Memasukkan Kode Voucher 3 (Minimal Order 100rb)");
                        harga = (belanja + ongkosKirim);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : 0");
                        Console.WriteLine($"Diskon Belanja  : 0");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                    else
                    {
                        Console.WriteLine("Gagal Memasukkan Kode Voucher (Kode voucher tidak ditemukan)");
                        harga = (belanja + ongkosKirim);
                        Console.WriteLine($"Belanja         : {belanja}");
                        Console.WriteLine($"Ongkos Kirim    : {ongkosKirim}");
                        Console.WriteLine($"Diskon Ongkir   : 0");
                        Console.WriteLine($"Diskon Belanja  : 0");
                        Console.WriteLine($"Total Belanja   : {harga}");
                    }
                }
                else
                {
                    Console.WriteLine("Masukan data yang benar");
                }
            }
           
            else
            {
                Console.WriteLine("Masukan data yang benar");
            } 

        }

        static void soal3()
        {
            Console.WriteLine("--Soal 3--");

            Console.Write("Belanja : ");
            int belanja = int.Parse(Console.ReadLine());
            Console.Write("Jarak (km) : ");
            int jarak = int.Parse(Console.ReadLine());
            Console.Write("Kode Promo : ");
            string kode = Console.ReadLine().ToUpper();

            double diskon = 0.4;
            int ongkir = 5000;
            int ongkirPlus = 1000;

            if (belanja >= 30000 && kode == "JKTOVO")
            {

                if (jarak > 0 && jarak <=5 )
                {
                    double potHarga = belanja * diskon;
                    Console.WriteLine($"Belanja         : {belanja}");
                    Console.WriteLine($"Diskon 40%      : {potHarga}");
                    Console.WriteLine($"Ongkir          : {ongkir}");
                    double totHarga = (belanja + ongkir) - potHarga;
                    Console.WriteLine($"Total Belanja   : {totHarga}");
                }
                else if(jarak > 5)
                {
                    double potHarga = belanja * diskon;
                    int jarakMax = jarak - 5;
                    int totOngkir = jarakMax * ongkirPlus + ongkir;
                    Console.WriteLine($"Belanja         : {belanja}");
                    Console.WriteLine($"Diskon 40%      : {potHarga}");
                    Console.WriteLine($"Ongkir          : {totOngkir}");
                    double totHarga = (belanja + totOngkir) - potHarga;
                    Console.WriteLine($"Total Belanja   : {totHarga}");

                }
                else
                {
                    Console.WriteLine("ERROR! Masukan Jarak Yang Benar");
                }
            }
            else
            {
                if (jarak > 0 && jarak <= 5)
                {
                    Console.WriteLine($"Belanja         : {belanja}");
                    Console.WriteLine($"Diskon 0%       : 0");
                    Console.WriteLine($"Ongkir          : {ongkir}");
                    int totHarga = belanja + ongkir;
                    Console.WriteLine($"Total Belanja   : {totHarga}");
                }
                else if(jarak > 5)
                {
                    int jarakMax = jarak - 5;
                    int totOngkir = jarakMax * ongkirPlus + ongkir;
                    Console.WriteLine($"Belanja         : {belanja}");
                    Console.WriteLine($"Diskon 0%       : 0");
                    Console.WriteLine($"Ongkir          : {totOngkir}");
                    double totHarga = belanja + totOngkir;
                    Console.WriteLine($"Total Belanja   : {totHarga}");
                }
                else
                {
                    Console.WriteLine("ERROR! Masukan Jarak Yang Benar");
                }

            }


        }

        static void soal2()
        {
            Console.WriteLine("--Soal 2--");

            int pulsa, point = 0;

            Console.Write("masukkan pulsa yang anda beli = Rp.");
            pulsa = int.Parse(Console.ReadLine());

            if (pulsa >= 10000 && pulsa < 25000)
            {
                point += 80;
                Console.WriteLine($"Pulsa : Rp.{pulsa}");
                Console.WriteLine($"Point : {point}");
            }
            else if (pulsa >= 25000 && pulsa < 50000)
            {
                point += 200;
                Console.WriteLine($"Pulsa : Rp.{pulsa}");
                Console.WriteLine($"Point : {point}");
            }
            else if (pulsa >= 50000 && pulsa <= 100000)
            {
                point += 400;
                Console.WriteLine($"Pulsa : Rp.{pulsa}");
                Console.WriteLine($"Point : {point}");
            }
            else if (pulsa > 100000)
            {
                point += 800;
                Console.WriteLine($"Pulsa : Rp.{pulsa}");
                Console.WriteLine($"Point : {point}");
            }
            else
            {
                Console.WriteLine("Anda tidak mendapatkan point");
            }

        }

        static void soal1()
        {
            Console.WriteLine("--soal 1--");

            int nilai;

            Console.Write("Masukkan Nilai Anda = ");
            nilai = int.Parse(Console.ReadLine());

            if (nilai >= 90 && nilai <= 100)
            {
                Console.WriteLine("anda mendapatkan grade A");
            }
            else if (nilai >= 70 && nilai <= 89)
            {
                Console.WriteLine("anda mendapatkan grade B");
            }
            else if (nilai >= 50 && nilai <= 69)
            {
                Console.WriteLine("anda mendapatkan grade C");
            }
            else if (nilai >= 0 && nilai <= 49)
            {
                Console.WriteLine("anda mendapatkan grade D");
            }
            else
            {
                Console.WriteLine("masukkan angka yang benar !");
            }
        }
    }
}
