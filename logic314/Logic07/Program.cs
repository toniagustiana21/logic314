﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;

namespace Logic07
{
    class Mamalia
    {
        public virtual void pindah()
        {
            Console.WriteLine("Kucing Lari.....");
        }
    }
    class Kucing : Mamalia
    {

    }
    class Paus : Mamalia
    {
        public override void pindah()
        {
            Console.WriteLine("Paus Berenang.....");
        }

    }
    internal class Program
    {
        //static List<User> listUser = new List<User>();
        static void Main(string[] args)
        {
            //bool ulangi = true;

            //while (ulangi)
            //{

            //    insertUser();

            //}
            //listUsers();
            //dateTime();
            //stringDateTime();
            //timeSpan();
            //classInheritance();

            overriding();
           
            Console.ReadKey();
        }

        static void overriding()
        {
            Paus paus = new Paus();
            paus.pindah();

            Kucing kucing = new Kucing();
            kucing.pindah();
        }

        static void classInheritance()
        {
            TypeMobil typeMobil = new TypeMobil();
            typeMobil.kecepatan = 100;
            typeMobil.posisi = 50;
            typeMobil.Civic();

        }


        static void timeSpan()
        {
            DateTime date1 = new DateTime(2023, 1, 1, 1, 0,0);
            DateTime date2 = DateTime.Now;

            TimeSpan span = date2.Subtract(date1);
            //TimeSpan span = date2 - date1;

            Console.WriteLine($"Total Hari : {span.Days}");
            Console.WriteLine($"Total Hari : {span.TotalDays}");
            Console.WriteLine($"Interval Jam : {span.Hours}");
            Console.WriteLine($"Total Jam : {span.TotalHours}");
            Console.WriteLine($"Interval Menit : {span.Minutes}");
            Console.WriteLine($"Total Menit : {span.TotalMinutes}");
            Console.WriteLine($"Inerval Detik : {span.Seconds}");
            Console.WriteLine($"Total Detik : {span.TotalSeconds}");

            


        }

        static void stringDateTime()
        {
            Console.WriteLine("===String Date Time===");

            Console.Write("Masukkan Tanggal (dd/mm/yyyy) : ");
            string strTanggal = Console.ReadLine();

            DateTime tanggal = DateTime.Parse(strTanggal);

            Console.WriteLine($"Tanggal : {tanggal.Day}");
            Console.WriteLine($"Bulan : {tanggal.Month}");
            Console.WriteLine($"Tahun : {tanggal.Year}");
            Console.WriteLine($"DayOfWeek : {(int)tanggal.DayOfWeek}");
            Console.WriteLine($"DayOfWeek : {tanggal.DayOfWeek}");
        }

        static void dateTime()
        {
            Console.WriteLine("===Date Time===");

            DateTime dt1 = new DateTime();
            Console.WriteLine(dt1);

            DateTime dt2 = DateTime.Now;
            Console.WriteLine(dt2);

            DateTime dt3 = DateTime.Now.Date;
            Console.WriteLine(dt3);

            DateTime dt4 = DateTime.UtcNow;
            Console.WriteLine(dt4);

            DateTime dt5 = new DateTime(2023, 03, 09);
            Console.WriteLine(dt5);

            DateTime dt6 = new DateTime(2023, 3, 9, 11, 45, 00);
            Console.WriteLine(dt6);

        }

        static void insertUser()
        {
            Console.WriteLine("===insert User===");

            Console.Write("Masukkan Nama : ");
            string nama = Console.ReadLine();
            Console.Write("Masukkan Umur : ");
            int umur = int.Parse(Console.ReadLine());
            Console.Write("Masukkan Alamat : ");
            string alamat = Console.ReadLine();

            List<User> listUser = new List<User>();
            User user = new User();
            user.Nama = nama;
            user.Umur = umur;
            user.Alamat = alamat;

            listUser.Add(user);

            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine($"Nama : {listUser[i].Nama}\tUmur : {listUser[i].Umur}\tAlamat : {listUser[i].Alamat}");
            }

        }

        static void listUsers()
        {
            Console.WriteLine("===List User===");
            List<User> listUser = new List<User>()
            {
                new User(){Nama = "Firdha", Umur = 24, Alamat = "Tangsel"},
                new User(){Nama = "Isni", Umur = 22, Alamat = "Cimahi"},
                new User(){Nama = "Asti", Umur = 23, Alamat = "Garut"},
                new User(){Nama = "Muafa", Umur = 22, Alamat = "Bogor"},
                new User(){Nama = "Toni", Umur = 24, Alamat = "Garut"}
            };

            

            for(int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine($"{listUser[i].Nama} sudah berumur {listUser[i].Umur} tahun dan alamatnya di {listUser[i].Alamat}");
            }
            

        }
    }
}
