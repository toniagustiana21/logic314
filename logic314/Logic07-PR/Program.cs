﻿using System;
using System.Globalization;
using System.Linq;

namespace Logic07_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {

            //soal1();
            //soal2();
            //soal3();
            //soal4();
            //soal5();
            //soal6();
            //soal7();
            //soal8();
            //soal9();
            //soal10();
            //soal11();
            //soal12();
            Console.ReadKey();
        }

        static void soal12()
        {
            Console.WriteLine("---Soal Nomor 12---");
            Console.WriteLine();
            Console.Write("Masukkan banyak deret : ");
            int deret = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int[] angka = new int[deret];
            int x = 0;
            int no = 1;

            while (x < deret)
            {
                Console.Write($"Masukkan data ke-{no++} : ");
                angka[x] = int.Parse(Console.ReadLine());
                x++;
            }
            Console.Write("\n");

            int tmp = 0;

            for (int i = 0; i < angka.Length; i++)
            {
                for (int j = 0; j < angka.Length - 1; j++)

                    if (angka[j] > angka[j + 1])
                    {
                        tmp = angka[j];
                        angka[j] = angka[j + 1];
                        angka[j + 1] = tmp;
                    }
                Console.WriteLine(string.Join(",", angka));

            }

            Console.Write("\n");

            foreach (int item in angka)
            {
                Console.Write(item + " ");
            }
        }

        static void soal11()
        {
            Console.Write("Masukkan input angka (pakai koma) = ");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Masukkan rotasi = ");
            int rotasi = int.Parse(Console.ReadLine());

            for (int i = 0; i < rotasi; i++)
            {
                int tampung = arr[0];
                for (int j = 0; j < arr.Length - 1; j++)
                {
                    arr[j] = arr[j + 1];
                }
                arr[arr.Length - 1] = tampung;
            }

            Console.Write($"Rotasi {rotasi} = ");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i]);
                if (i != arr.Length - 1)
                {
                    Console.Write(",");
                }
            }
        }

        static void soal10()
        {
            Console.WriteLine("---Soal Nomor 10---");
            Console.Write("Masukan sampel input(Pakai Koma)= ");
            int[] input = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            int nilaiMax = 0;
            int hitung = 0;
            //for (int i = 0; i < input.Length; i++)
            //{

            //    if (input[i] >= nilaiMax)
            //    {
            //        nilaiMax = input[i];
            //    }
            //}

            //for (int j = 0; j < input.Length; j++)
            //{
            //    if (input[j] == nilaiMax)
            //    {
            //        hitung += 1;
            //    }
            //}

            for (int j = 0; j < input.Length; j++)
            {
                if (input[j] >= nilaiMax)
                {
                    nilaiMax = input[j];
                }
                if (input[j] == nilaiMax)
                {
                    hitung += 1;
                }
            }

            Console.WriteLine(hitung);
        }


        static void soal9()
        {
            Console.WriteLine("===Soal (Diagonal Difference) ===");

            int[,] matrix = new int[,]
            {
                {11, 2, 4},
                {4, 5, 6},
                {10, 8, -12}
            };

            string tampungDiagonal1 = "";

            int primaryDiagonalSum = 0;

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                primaryDiagonalSum += matrix[i, i];
                //tampungDiagonal1 += tampungDiagonal1 == "" ? matrix[i, i].ToString() : matrix[i,i] < 0 ? " - " + Math.Abs(matrix[i, i]).ToString() :  " + " + matrix[i, i].ToString();
                if (tampungDiagonal1 == "")
                {
                    tampungDiagonal1 += matrix[i, i].ToString();
                }
                else if (matrix[i, i] < 0)
                {
                    tampungDiagonal1 += " - " + Math.Abs(matrix[i, i]).ToString();
                }
                else
                {
                    tampungDiagonal1 += " + " + matrix[i, i].ToString();
                }
            }

            string tampungDiagonal2 = "";
            int secondaryDiagonalSum = 0;

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                secondaryDiagonalSum += matrix[i, matrix.GetLength(0) - 1 - i];
                tampungDiagonal2 += tampungDiagonal2 == "" ? matrix[i, matrix.GetLength(0) - 1 - i].ToString() : " + " + matrix[i, matrix.GetLength(0) - 1 - i].ToString();
            }

            int diagonalDiffrence = primaryDiagonalSum - secondaryDiagonalSum;



            Console.WriteLine($"Diagonal 1 : {tampungDiagonal1} = {primaryDiagonalSum}");
            Console.WriteLine($"Diagonal 2 : {tampungDiagonal2} = {secondaryDiagonalSum}");
            Console.WriteLine($"Perbedaan Diagonal = {primaryDiagonalSum} - {secondaryDiagonalSum} = {diagonalDiffrence}");
        }

        static void soal8()
        {
            Console.WriteLine("===Soal 8 (Star Case)===");

            Console.Write("Masukkan Input : ");
            int input = int.Parse(Console.ReadLine());

            for(int i = 1; i <= input; i++)
            {
                for(int j = i; j <= input; j++)
                {
                    Console.Write(" ");
                }
                for(int k = 1; k <= i; k++)
                {
                    Console.Write("*");
                }
                Console.Write("\n");
            }

        }

        static void soal7()
        {
            Console.WriteLine("Soal 7 (Elsa dan Dimas)");

            Console.Write("Jumlah Makanan Yang Di Pesan = ");
            int jmlMakanan = int.Parse(Console.ReadLine());
            Console.Write("Index Makanan Yang Tidak Di Makan Elsa = ");
            int indexMakanan = int.Parse(Console.ReadLine());
            Console.Write("Jumlah Harga Setiap Menu Makanan(Pisahkan dengan koma) = ");
            int[] menuMakanan = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Masukan Uang Elsa = ");
            int uangElsa = int.Parse(Console.ReadLine());
            int totalMakan = 0;

            for (int i = 0; i < menuMakanan.Length; i++)
            {
                totalMakan += menuMakanan[i];
            }

            int total = totalMakan - menuMakanan[indexMakanan];
            int totalBayar2 = total / 2;

            int sisa = uangElsa - totalBayar2;

            Console.WriteLine($"Elsa harus membayar = {totalBayar2}");

            if (sisa > 0)
            {
                Console.WriteLine($"Sisa uang elsa = {sisa}");
            }
            else if (sisa == 0)
            {
                Console.WriteLine("Uang Pas");
            }
            else
            {
                Console.WriteLine($"Uang elsa kurang = {sisa}");
            }

        }

        static void soal6()
        {
            Console.WriteLine("===Soal 6===");

            Console.Write("Masukkan input = ");
            string input = Console.ReadLine();
            //char[] chars = input.ToCharArray();

            for(int i = 0; i < input.Length; i++)
            {
                Console.WriteLine("***" + input[i] + "***");
            }
        }

        static void soal5()
        {
            Console.WriteLine("===Soal 5 (Jumlah Vokal dan Konsonan)===");

            Console.Write("Masukkan Kalimat : ");
            string kata = Console.ReadLine().ToLower();
            char[] n = kata.ToCharArray();
            int vokal = 0;
            int konsonan = 0;
            


            for (int i = 0; i < n.Length; i++)
            {
                if (n[i] == 'a' || n[i] == 'i' || n[i] == 'u' || n[i] == 'e' || n[i] == 'o')
                {
                    vokal++;
                }

                else if (n[i] != 'a' && n[i] != 'i' && n[i] != 'u' && n[i] != 'e' && n[i] != 'o' && n[i] != ' ')
                {
                    konsonan++;
                }

            }
            Console.WriteLine($"Jumlah Huruf Vokal : {vokal}");
            Console.WriteLine($"Jumlah Huruf konsonan : {konsonan}");
        }

        static void soal4()
        {
            Console.WriteLine("Soal 4 (FTI)");

            Console.Write("Masukkan Tanggal Mulai (dd/mm/yyyy) : ");
            DateTime tanggalMulai = DateTime.Parse(Console.ReadLine());
            Console.Write("masukkan berapa hari lagi lama waktu akan ujian : ");
            int exam = int.Parse(Console.ReadLine());
            Console.Write("Masukkan tanggal libur : ");
            int[] libur = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            DateTime tanggalSelesai = tanggalMulai.AddDays(-1);

            for(int i = 0; i < exam; i++)
            {
                tanggalSelesai = tanggalSelesai.AddDays(1);
                if(tanggalSelesai.DayOfWeek == DayOfWeek.Saturday)
                {
                    tanggalSelesai = tanggalSelesai.AddDays(2);
                }
                for(int j = 0; j < libur.Length; j++)
                {
                    if(tanggalSelesai.Day == libur[j])
                    {
                        tanggalSelesai = tanggalSelesai.AddDays(1);
                        if(tanggalSelesai.DayOfWeek == DayOfWeek.Saturday)
                        {
                            tanggalSelesai = tanggalSelesai.AddDays(2);
                        }
                    }
                }
            }
            DateTime tanggalUjian = tanggalSelesai.AddDays(1);
            if(tanggalUjian.DayOfWeek == DayOfWeek.Saturday)
            {
                tanggalUjian = tanggalUjian.AddDays(2);
            }
            Console.Write("\n");
            Console.WriteLine("kelas akan ujian pada tanggal = " + tanggalUjian.ToString("dddd, dd/MM/yyyy"));
        }





        static void soal3()
        {
            Console.WriteLine("===Soal 3 (Peminjaman Buku)");

            Console.Write("Masukkan Tanggal Peminjaman (dd/mm/yyyy) : ");
            string tglPinjam = Console.ReadLine();

            Console.Write("Masukkan Tanggal Pengembalian (dd/mm/yyyy) : ");
            string tglBalik = Console.ReadLine();

            DateTime dateTanggalP = DateTime.Parse(tglPinjam);
            DateTime dateTanggalB = DateTime.Parse(tglBalik);
            
            int denda = 0;


            TimeSpan span = dateTanggalB.Subtract(dateTanggalP);

            if(span.Days > 3)
            {
                denda = (span.Days - 3) * 500;
                Console.WriteLine($"Anda Telat Mengembalikan Buku {span.Days - 3} Hari");
                Console.WriteLine($"Denda Yang Harus dibayar : {denda}");
            }
            else
            {
                Console.WriteLine("Terima Kasih Telah Mengembalikan Buku Tepat Waktu");
            }

            
            
        }

        static void soal2()
        {
            Console.WriteLine("===Soal 2 (Sinyal Bahaya)===");

            Console.Write("Masukkan Sinyal Bahaya : ");
            string kalimat = Console.ReadLine().ToUpper();
            char[] karakter = kalimat.ToCharArray();

            int b = 0;
            int s = 0;

            for(int i = 0; i < karakter.Length; i+=3) 
            {
                if (karakter[i] == 'S' && karakter[i+1] == 'O' && karakter[i+2] == 'S')
                {
                    b += 1;
                }
                if (karakter[i] != 'S' || karakter[i+1] != 'O' || karakter[i+2] != 'S')
                {
                    s+= 1;
                }
                
            }
            Console.WriteLine($"jumlah sinyal yang salah adalah : {s}");
            Console.WriteLine($"jumlah sinyal yang benar adalah : {b}");
            
            

        }



        static void soal1()
        {
            Console.WriteLine("===Soal 1===");

            Console.Write("Masukkan Jumlah Orang : ");
            int x = int.Parse(Console.ReadLine());

            int fakt = 1;

            Console.Write($"{x}! = ");

            for(int i = x; i >= 1; i--)
            {
                Console.Write(i);
                fakt  = fakt * i;
                if(i > 1)
                {
                    Console.Write(" * ");
                }
                
            }
            Console.Write($" = {fakt}");
            Console.WriteLine("\nAda " + fakt + " cara");
        }
    }
}
