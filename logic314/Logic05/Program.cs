﻿using System;

namespace Logic05
{
    internal class Program
    {
        static void Main(string[] args)
        {

            contain();
            //padLeft();
            //convertArrayAll();
            Console.ReadKey();
        }

        static void convertArrayAll()
        {
            Console.WriteLine("===Pad Left===");

            Console.Write("Masukkan angka array : ");
            int[] array = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            Console.WriteLine(string.Join("\t", array));
        }

        static void padLeft()
        {
            Console.WriteLine("===Pad Left===");

            Console.Write("Masukkan Kalimat : ");
            int input = int.Parse(Console.ReadLine());

            Console.Write("Masukkan panjang karakter : ");
            int panjang = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Karakter : ");
            char chars = char.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil Pad Left : {input.ToString().PadLeft(panjang, chars)}");
        }

        static void contain()
        {
            Console.WriteLine("===Contain===");

            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            Console.Write("Masukkan contain : ");
            string contain = Console.ReadLine();

            if (kalimat.Contains(contain))
            {
                Console.WriteLine($"kalimat ({kalimat}) ini mengandung {contain}");
            }
            else
            {
                Console.WriteLine($"kalimat ({kalimat}) ini tidak mengandung {contain}");
            }
        }
    }
}
