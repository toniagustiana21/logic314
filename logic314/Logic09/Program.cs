﻿using System;

namespace Logic09
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Soal1();
            //Soal2();
            //Soal3();
            //Soal3_2();
            Soal3_3();
            Console.ReadKey();
        }

        static void Soal3_3()
        {
            Console.WriteLine("===Soal 3 (Pola)===");

            Console.Write("Masukkan nilai suku : ");
            int suku = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Angka Pertama : ");
            int angka = int.Parse(Console.ReadLine());

            for(int i = 0; i < suku; i++)
            {
                int x = 0;
                for(int j = 0; j < suku; j++)
                {
                    x += angka;
                    if(j < suku -1 - i)
                    {
                        Console.Write("\t");
                    }
                    else
                    {
                        Console.Write(x.ToString() + "\t");
                    }

                }
                Console.WriteLine();
            }
        }

        static void Soal3_2()
        {
            Console.WriteLine("===Soal 3 (Pola)===");

            Console.Write("Masukkan nilai suku : ");
            int suku = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Angka Pertama : ");
            int angka = int.Parse(Console.ReadLine());


            for(int i = 0; i < suku; i++)
            {
                int x = 0;
                for(int j = 0; j < suku; j++)
                {
                    x += angka;
                    if (j == 0 || i == suku-1 || i == j)
                        Console.Write(x.ToString() + "\t");
                    else
                        Console.Write("\t");
                }
                Console.WriteLine();
            }
        }

        static void Soal3()
        {
            Console.WriteLine("===Soal 3 (Pola)===");

            Console.Write("Masukkan nilai suku : ");
            int suku = int .Parse(Console.ReadLine());

            Console.Write("Masukkan Angka Pertama : ");
            int angka = int .Parse(Console.ReadLine());
           
            int hasil = 0;
            //int rasio = angka;
           

            for(int i = 0; i < suku; i++)
            {
                for(int j = 0; j <= i; j++)
                {
                    if(j == 0)
                    {
                        hasil = angka;
                        Console.Write($"{hasil}\t");
                    }
                    else if(i == j)
                    {
                        hasil += angka*j;
                        Console.Write($"{hasil}\t");
                    }
                    else if(i == suku - 1)
                    {
                        hasil = hasil + angka;
                        Console.Write($"{hasil}\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine();
            }
        }

        static void Soal2()
        {
            double terigu = 115;
            double gulaPasir = 190;
            double susu = 100;

            Console.Write("Masukkan jumlah kue pukis : ");
            int pukis = int.Parse(Console.ReadLine());

            Console.WriteLine($"Terigu {Math.Round(terigu / 15 * pukis)} gram");
            Console.WriteLine($"Gula Pasir {Math.Round(gulaPasir / 15 * pukis)} gram");
            Console.WriteLine($"Susu {Math.Round(susu / 15 * pukis)} ml");
        }

        static void Soal1()
        {
            Console.WriteLine("===Soal 1 (Supir Ojol)===");

            Console.Write("Masukkan Input : ");
            int input = int.Parse(Console.ReadLine());

            Console.Write("Masukkan Total Jarak (Pakai spasi) : ");
            double[] jarak = Array.ConvertAll(Console.ReadLine().Split(" "), double.Parse);

            double bensin = 2.5;
            double totJarak = 0;
            double hasil = 0;            

            string tampung = "";

            if(input > 0 && input < 5)
            {
                for(int i = 0; i < input; i++)
                {
                    totJarak += jarak[i];
                    hasil += jarak[i] / bensin;

                    if(i == 0)
                    {
                        tampung += jarak[i].ToString() + "km ";
                    }
                    else
                    {
                        tampung += " + " + jarak[i].ToString() + "km ";
                    }
                }
                Console.WriteLine($"Jarak Tempuh : {tampung} = {totJarak}km");
                Console.WriteLine($"Bensin = {Math.Round(hasil)} Liter");
                
            }
            else
            {
                Console.WriteLine("Error!");
            }

        }
    }
}
