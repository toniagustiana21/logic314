create database db_penerbit

use db_penerbit

create table tblPengarang(
ID bigint primary key identity (1,1),
Kd_Pengarang varchar(7) not null,
Nama varchar(30) not null,
Alamat varchar(80) not null,
Kota varchar(15) not null,
Kelamin varchar(1) not null
)

insert into tblPengarang(Kd_Pengarang, Nama, Alamat, Kota, Kelamin)
values
('P0001','Ashadi','Jl. Beo 25','Yogya','P'),
('P0002','Rian','Jl. Solo 123','Yogya','P'),
('P0003','Suwardi','Jl. Semangka 13','Bandung','P'),
('P0004','Siti','Jl. Durian 15','Solo','W'),
('P0005','Amir','Jl. Gajah 33','Kudus','P'),
('P0006','Suparman','Jl. Harimau 25','Jakarta','P'),
('P0007','Jaja','Jl. Singa 7','Bandung','P'),
('P0008','Saman','Jl. Naga 12','Yogya','P'),
('P0009','Anwar','Jl. Tidar 6A','Magelang','P'),
('P0010','Fatmawati','Jl. Renjana 4','Bogor','W')

select * from tblPengarang

create table tblGaji(
ID bigint primary key identity(1,1),
Kd_Pengarang varchar(7) not null,
Nama varchar(30) not null,
Gaji decimal(18,4) not null
)

insert into tblGaji(Kd_Pengarang, Nama, Gaji)
values
('P0002','Rian',600000),
('P0005','Amir',700000),
('P0004','Siti',500000),
('P0003','Suwardi',1000000),
('P0010','Fatmawati',600000),
('P0008','Saman',750000)

select * from tblGaji

--soal no 1 (hitung dan tampilkan jumlah pengarang)
select count(ID) as jmlPengarang from tblPengarang

--soal no 2 (jumlah pengarang wanita dan pria)
select kelamin, 
case 
	when kelamin='P' then count(kelamin)
	when kelamin='W' then count(kelamin)
end as jmlPengarang
from tblPengarang
group by Kelamin

--soal no 2 cara ke 2
select kelamin, count(kelamin) as jmlKelamin
from tblPengarang group by kelamin

--soal no 3 (tampilkan record kota dan jumlah kota)
select kota, count(kota) as jmlKota from tblPengarang group by kota

--soal no 4 (record kota diatas 1 kota)
select kota, count(kota) as jmlKota
from tblPengarang
group by kota
having COUNT(Kota) > 1

--soal no 5 ()
select max(kd_pengarang) as terbesar, min(kd_pengarang) as terendah from tblPengarang

--soal no 5 cara ke 2
select
(select top 1 kd_pengarang from tblPengarang order by kd_pengarang desc) as tertinggi,
(select top 1 kd_pengarang from tblPengarang order by kd_pengarang asc) as terendah


--soal no 6
select min(gaji) as terendah, max(gaji) as etrtinggi from tblGaji

--soal no 7 (gaji diatas 600.000)
select Gaji from tblGaji
group by gaji
having gaji > 600000

--soal 7 cara ke 2
select gaji from tblGaji where Gaji > 600000

--soal no 8 (tampilkan jumlah gaji)
select sum(gaji) as jmlGaji from tblGaji

--soal no 9 (jumlah gaji berdasarkan kota)
select pengarang.kota, sum(Gaji) as TotalGaji from tblGaji as jmlGaji 
join tblPengarang as pengarang
on jmlGaji.Kd_Pengarang = pengarang.Kd_Pengarang
group by pengarang.Kota

--soal no 10 (record pengarang antara p0003 - p0006)
select kd_pengarang, Nama from tblPengarang where id > 2 and id < 7

select * from tblPengarang where kd_pengarang between 'p0003' and 'p0006'

select * from tblPengarang where Kd_Pengarang >= 'p0003' and Kd_Pengarang != 'p0006'

--soal no 11 (seluruh data yogya, solo dan magelang)
select * from tblPengarang where kota='yogya' or kota='solo' or kota='magelang'

select * from tblPengarang where kota in('yogya','solo','magelang')

--soal no 12 (tampilkan seluruh data yang bukan kota yogya)
select * from tblPengarang where kota !='yogya'

select * from tblPengarang where not kota = 'yogya'

--soal no 13 (a. dimulai dengan hurup a)
select * from tblPengarang where Nama like 'a%'

--soal no 13 (b. berakhiran i)
select * from tblPengarang where nama like '%i'

--soal no 13 (c. huruf ketiganya a)
select * from tblPengarang where nama like '__a%'


--soal no 13 (d. tidak berakhiran n)
select * from tblPengarang where nama not like '%n'

select * from tblPengarang where not nama like '%n'


--soal no 14 (tampilkan seluruh data tblPengarang dan tblGaji dengan kd_pengarang yang sama)
select p.Kd_Pengarang, g.nama, p.Alamat, p.Kota, g.Gaji from tblPengarang as p 
join tblGaji as g 
on p.Kd_Pengarang = g.Kd_Pengarang

select * from tblPengarang as p 
join tblGaji as g 
on p.Kd_Pengarang = g.Kd_Pengarang

--soal no 15 (kota yang memiliki jagi < 1.000.000)
select g.Gaji, p.kota from tblGaji as g 
join tblPengarang as p 
on g.Kd_Pengarang = p.Kd_Pengarang
where g.Gaji < 1000000

select * from tblGaji as g 
join tblPengarang as p 
on g.Kd_Pengarang = p.Kd_Pengarang
where g.Gaji < 1000000

--soal no 16 (ubah panjang dari tipe kelamin)
alter table tblPengarang alter column kelamin varchar(10) not null

--soal no 17 (tambah kolom)
alter table tblPengarang add Gelar varchar(12)

select * from tblPengarang

--soal no 18 (ubah data alamat dan kota)
update tblPengarang set alamat='Jl. Cendrawasih 65', kota='Pekanbaru' where Nama='Rian'

--soal no 19(buat view dengan nama vwPengarang)
create view vwPengarang
as
select p.kd_pengarang, p.Nama, p.Kota, g.Gaji from tblPengarang as p join tblGaji as g
on p.Kd_Pengarang = g.Kd_Pengarang

select * from tblPengarang as p full join tblGaji as g
on p.Kd_Pengarang = g.Kd_Pengarang

select * from vwPengarang

drop view vwPengarang



