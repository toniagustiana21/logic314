--TUGAS SQL DAY 02


create database DB_Entertainer

use db_entertainer

create table artis(
kd_artis varchar(100) primary key,
nm_artis varchar (100) not null,
jk varchar(100) not null,
bayaran int not null,
award int not null,
negara varchar (100) not null
)

alter table artis alter column bayaran bigint not null

insert into artis(kd_artis, nm_artis, jk, bayaran, award, negara)
values
--('A001', 'ROBERT DOWNEY', 'PRIA', 3000000000, 2, 'AS')
('A002', 'ANGELINA JOLIE', 'WANITA', 700000000, 1, 'AS'),
('A003', 'JACKIE CHAN', 'PRIA', 2000000000, 7, 'HK'),
('A004', 'JOE TASLIM', 'PRIA', 350000000, 1, 'ID'),
('A005', 'CHELSEA ISLAN', 'WANITA', 300000000, 0, 'ID')

update artis set bayaran=200000000 where kd_artis='a003'

select * from artis

create table film(
kd_film varchar(10) primary key,
nm_film varchar(55) not null,
genre varchar(55) not null,
artis varchar(55) not null,
produser varchar(55) not null,
pendapatan bigint not null,
nominasi int not null
)

create table produser(
kd_produser varchar(50) primary key,
nm_produser varchar(50),
international varchar(50)
)

create table negara(
kd_negara varchar(100) primary key,
nm_negara varchar(100)
)

create table genre(
kd_genre varchar(50) primary key,
nm_genre varchar(50)
)

insert into film(kd_film, nm_film, genre, artis, produser, pendapatan, nominasi)
values
('F001', 'IRON MAN', 'G001', 'A001', 'PD01', 2000000000, 3),
('F002', 'IRON MAN 2', 'G001', 'A001', 'PD01', 1800000000, 2),
('F003', 'IRON MAN 3', 'G001', 'A001', 'PD01', 1200000000, 0),
('F004', 'AVENGER:CIVIL WAR', 'G001', 'A001', 'PD01', 2000000000, 1),
('F005', 'SPIDERMAN HOME COMING', 'G001', 'A001', 'PD01', 1300000000, 0),
('F006', 'THE RAID', 'G001', 'A004', 'PD03', 800000000, 5),
('F007', 'FAST & FURIOUS', 'G001', 'A004', 'PD05', 830000000, 2),
('F008', 'HABIBIE DAN AINUN', 'G004', 'A005', 'PD03', 670000000, 4),
('F009', 'POLICE STORY', 'G001', 'A003', 'PD02', 700000000, 3),
('F010', 'POLICE STORY 2', 'G001', 'A003', 'PD02', 710000000, 1),
('F011', 'POLICE STORY 3', 'G001', 'A003', 'PD02', 615000000, 0),
('F012', 'RUSH HOUR', 'G003', 'A003', 'PD05', 695000000, 2),
('F013', 'KUNGFU PANDA', 'G003', 'A003', 'PD05', 923000000, 5)

SELECT * from film

insert into produser(kd_produser, nm_produser, international)
values
('PD01', 'MARVEL', 'YA'),
('PD02', 'HONGKONG CINEMA', 'YA'),
('PD03', 'RAPI FILM', 'TIDAK'),
('PD04', 'PARKIT', 'TIDAK'),
('PD05', 'PARAMOUNT PICTURE', 'YA')

insert into negara(kd_negara, nm_negara)
values
('AS', 'AMERIKA SERIKAT'),
('HK', 'HONGKONG'),
('ID', 'INDONESIA'),
('IN', 'INDIA')

insert into genre(kd_genre, nm_genre)
values
('G001', 'ACTION'),
('G002', 'HORROR'),
('G003', 'COMEDY'),
('G004', 'DRAMA'),
('G005', 'THRILLER'),
('G006', 'FICTION')

--soal no 1
select produser.nm_produser, sum(film.pendapatan) from film 
join produser on film.produser = produser.kd_produser
where produser.nm_produser like '%l'
group by produser.nm_produser

--soal no 2
select nm_film, nominasi from film where nominasi = 0

--soal no 3
select nm_film from film where nm_film like 'p%'

--soal no 4
select nm_film from film where nm_film like '%y'

--soal no 5
select nm_film from film where nm_film like'%d%'

--soal no 6
select film.nm_film, artis.nm_artis from film join artis
on film.artis = artis.kd_artis

select nm_film, nm_artis from artis, film where artis = kd_artis

--soal no 7
select film.nm_film, artis.negara from film join artis
on film.artis = artis.kd_artis where negara='HK'

--soal no 8
select film.nm_film, negara.nm_negara
from 
film join artis on film.artis = artis.kd_artis
join negara on artis.negara = negara.kd_negara
where negara.nm_negara not like '%o%'

--soal no 9
select artis.nm_artis from film right join artis
on film.artis = artis.kd_artis where film.artis is null
--cara ke 2
select artis.nm_artis from artis left join film
on artis.kd_artis = film.artis 
group by artis.nm_artis
having COUNT(film.kd_film) < 1

--soal no 10
select artis.nm_artis, genre.nm_genre 
from artis join film on artis.kd_artis = film.artis
join genre on genre.kd_genre = film.genre
where genre.nm_genre like 'drama'

--soal no 11
select artis.nm_artis, genre.nm_genre 
from artis join film on artis.kd_artis = film.artis
join genre on genre.kd_genre = film.genre
where genre.nm_genre = 'action'
group by artis.nm_artis, genre.nm_genre

select distinct artis.nm_artis, genre.nm_genre 
from artis join film on artis.kd_artis = film.artis
join genre on genre.kd_genre = film.genre
where genre.nm_genre = 'action'

--soal no 12
select negara.kd_negara, negara.nm_negara, count(film.artis) jumlah_film 
from negara left join artis on negara.kd_negara = artis.negara
left join film on film.artis = artis.kd_artis
group by negara.kd_negara, negara.nm_negara 

--soal no 13
select film.nm_film 
from film join produser on film.produser = produser.kd_produser
where international not like 't%'

--soal no 14
select produser.nm_produser, count(film.produser) jml_film
from produser left join film on produser.kd_produser = film.produser
group by produser.nm_produser
order by produser.nm_produser asc


